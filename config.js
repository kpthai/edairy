//var HOST = "http://172.31.234.23";
//var HOST = "http://192.168.1.34:2890";
var HOST = "http://edairy.dpo.go.th:1962";
//var HOST = "http://localhost/htdocs";
var LOGIN_URL = HOST+"/api/login.php";
var PROFILE_URL = HOST+"/api/profile.php";
var COW_URL = HOST+"/api/cow.php";
var COW_DETAIL_URL = HOST+"/api/cow_detail.php";
var COW_STATUS_URL = HOST+"/api/cow_status.php";
var MASTER_DATA_URL = HOST+"/api/master_data.php";
var FARMER_URL = HOST+"/api/farmer.php";
var FARMER_PAYMENT_URL = HOST+"/api/farmer_payment.php";
var HEALTH_URL = HOST+"/api/health.php";
var MILK_QUALITY_URL = HOST+"/api/milk_quality.php";
var COW_MILK_URL = HOST+"/api/cow_milk.php";
var MILK_RECEIPT_URL = HOST+"/api/milk_receipt.php";
var POPULATION_SURVEY_URL = HOST+"/api/population_survey.php";
var STATUS_SURVEY_URL = HOST+"/api/status_survey.php";
var BREEDING_SERVICE_URL = HOST+"/api/breeding_service.php";
var BREEDING_DATA_URL = HOST+"/api/breeding_data.php";
var PREGNANT_URL = HOST+"/api/pregnant.php";
var DAIRY_URL = HOST+"/api/dairy.php";
var BIRTH_URL = HOST+"/api/birth.php";
var TREAT_RESULT_URL = HOST+"/api/treat_result.php";
var BREEDER_URL = HOST+"/api/breeder.php";
var COW_HAS_COW_STATUS_URL = HOST+"/api/cow_has_cow_status.php";
var FARM_URL = HOST+"/api/farm.php";

var ADD_CATTLE_URL = HOST+"/api/add_cattle.php";

var COW_PROJECT_URL = HOST+"/api/cow_project.php";
var REGION_URL = HOST+"/api/region_data.php";
var PROVINCE_URL = HOST+"/api/province_data.php";
var DISTRICT_URL = HOST+"/api/district_data.php";
var SUB_DISTRICT_URL = HOST+"/api/sub_district_data.php";

var FARMER_TRAINING_URL = HOST+"/api/farmer_training.php";


var WORK_ORDER_URL = HOST + "/api/MobileWorkOrder";
var LOGIN_MAS_URL = HOST + "/api/MobileLogin";
var GPS_TRACKING_URL = HOST + "/api/MobileGPS";
var CCP_URL = HOST + "/api/MobileCCP";
var INVENTORY_URL = HOST + "/api/MobileInventory";
var PM_URL = HOST + "/api/MobilePMCheckingList";
var CHECKINOUT_URL = HOST + "/api/MobileCheckInOut";
var OPERATION_URL = HOST + "/api/MobileOperation";
var CATALOG_URL = HOST + "/api/MobileCatalog";
var COMPONENT_URL = HOST + "/api/MobileComponent";
var ROUTING_URL = HOST + "/api/MobileRouting";

//SAP INBOUND PART
//var SOAP_PROXY_HOST = "http://172.31.234.23:7777";
//var SOAP_PROXY_HOST = "http://192.168.1.35:12116";
var SOAP_PROXY_HOST = "http://10.1.1.76:7777";
var SIMULATE_URL = SOAP_PROXY_HOST+"/SaleOrderSimulateAjax.svc/SaleOrderSimulate";
var SIMULATE_RESULT_URL = SOAP_PROXY_HOST+"/SaleOrderSimulateAjax.svc/getSimulateResult";
var REGISTER_DEVICE_URL = HOST+"/api/MobilePushNotification";

var SPARE_PART_REQUEST_URL = SOAP_PROXY_HOST+"/SparePartRequestAjax.svc/RequestSparePart";
