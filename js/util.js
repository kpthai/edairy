/**
 * 
 * 
 * @author Chanwoot  See-ngam <chanwoot2536@gmail.com>
 * @copyright 2017 Chanwoot See-ngam
 * @since 1.0
 * 
 */

/**
 * 
 * @param {type} params
 *      Example: {id:id_value, url: url_value}
 *  
 * @param {type} func_success : a success callback function
 * @param {type} func_error : an error callback function
 * 
 * @returns {undefined}
 */
function getJsonData(url,params,func_success,func_error){
    // verify the callback functions
    if((typeof func_success !== "function") || 
       (typeof func_error !== "function")){
        
        console.log("func_success or func_error is not defined or not functions");
        return ;
    }

    /*
    var url = params.url;
    var id = params.id;
    var cmd;
    
    if(typeof params.cmd !== 'undefined'){
        cmd = params.cmd;
    }else{
        cmd = "info";
    }*/
    
    //var posting = $.post(url,{id: id, cmd: cmd});
    var posting = $.post(url, params );    
    posting.done(function (data){
        var ret = data;
        if(ret.status == "ok"){
            // call the success function with the data
            func_success(ret.data);
        }else{
            // call the error function with the error detail
            func_error(ret.error_code, ret.error_desc);
        }
    });
    
    // failed
    posting.fail(function (data){
        var desc_en = "Sorry, your request cannot be process at the moment. Please try again later.";
        var desc_th = "ขออภัย ระบบไม่สามารถทำงานตามที่ท่านขอได้ กรุณาลองอีกครั้ง";
        var desc = desc_th;
        var error_code = -100;
        
        func_error(error_code, desc);
    });
}


var getUrlParameter = function getUrlParameter(sParam) {
    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : sParameterName[1];
        }
    }
}

function blockUI(e){
    var $el = $(e);
    $el.block({
        message: '<img src="/assets/img/ajax-loading.gif" align="">',
        centerY: true,
        css: {
            top: '10%',
            border: 'none',
            padding: '2px',
            backgroundColor: 'none'
        },
        overlayCSS: {
            backgroundColor: '#000',
            opacity: 0.05,
            cursor: 'wait'
        }
    });
}

function unblockUI(e){
    var $el = $(e);
    $el.unblock({
        onUnblock: function () {
            $el.removeAttr("style");
        }
    });
}

function enableObject(obj,enabled){
    if(enabled == true){
        obj.removeAttr("disabled");
    }else{
        obj.attr("disabled","disabled");
    }
}


/**
 * initialize the Select2 Ajax loader
 * 
 * ### HTML:
 * 
 * <select id="fname" name="fname" class="form-control" title="Please input a name of a user" >
 * </select>
 * 
 * 
 * ### Javascript: Single Selection (with NO default selection)
 * 
 * var placeholder = "Please input a name of a user";
 * var ajax_url = "/cms/account/?cmd=search";
 * var multiple = false;
 * 
 * var obj = $("#fname");
 * 
 * initSelect2Ajax(obj, placeholder, url, multiple);
 * 
 * 
 * ### Javascript: Single Selection (with a default selection)
 * 
 * var placeholder = "Please input a name of a user";
 * var ajax_url = "/cms/account/?cmd=search";
 * var multiple = false;
 * var init_data = [{'id':1, 'text':'Somchai'}];
 * 
 * var obj = $("#fname");
 * 
 * initSelect2Ajax(obj, placeholder, url, multiple, init_data);
 * 
 * 
 * ### Javascript: Multiple Selection (with NO default selection)
 * 
 * var placeholder = "Please input a name of a user";
 * var ajax_url = "/cms/account/?cmd=search";
 * var multiple = true;
 * 
 * var obj = $("#fname");
 * 
 * initSelect2Ajax(obj, placeholder, url, multiple);
 * 
 * 
 * ### Javascript: Multiple Selection (with a default selection)
 * 
 * var placeholder = "Please input a name of a user";
 * var ajax_url = "/cms/account/?cmd=search";
 * var multiple = true;
 * var init_data = [{'id':1, 'text':'Somchai'},{'id':2, 'text':'Somsri'}];
 * 
 * var obj = $("#fname");
 * 
 * initSelect2Ajax(obj, placeholder, url, multiple, init_data);
 * 
 * 
 * @param {jQuery Object} obj : a select2 jquery object
 * @param {String} placeholder : a placeholder string
 * @param {String} ajax_url : an server url providing JSON data
 * @param {Boolean} multiple : true, if multi-selection, or false if single selection
 * @param {Array} init_data : an initially selected data
 * @param {int} min_input : an initially minimumInputLength
 * @returns {undefined}
 */
function initSelect2Ajax(obj, placeholder, ajax_url, multiple, init_data, min_input,po_id){
    
    // set the default data group,  if any
    if(typeof min_input === 'undefined'){
        min_input = 1;
    }
    
    // set the default options
    var options = {
        placeholder: placeholder,
        allowClear: true,
        theme: "bootstrap",     // set theme style to Bootstrap
        ajax: {
            url: ajax_url,
            dataType: 'json',
            data: function (params) {
                return {
                    farm_name: params.term, // search term
                    po_id: po_id
                    //page: params.page
                };
            },
            processResults: function (json) {
                var data;
                if(json.status != "ok"){
                    return {
                        results: []     // not found, return empty result
                    };
                }

                data = json.data;
                return {
                    //results: data
                    results: $.map(data, function (item) {
                        var data = {
                            text: item.name,
                            id: item.id,
                            hint: item.hint
                        };
                        if (typeof(item.extra) !== 'undefined'){
                            data.extra = item.extra;
                        }else{
                            data.extra = null;
                        }
                        return data;
                    })
                };
            },
            cache: true
        },
        escapeMarkup: function (markup) {
            return markup;
        }, // let our custom formatter work
        minimumInputLength: min_input,
        templateResult: formatSelect2AjaxResult, // omitted for brevity, see the source of this page
        templateSelection: formatSelect2AjaxSelection // omitted for brevity, see the source of this page            
    };

    // if this is a multiple select2
    if(multiple === true){
        options.multiple = true;
    }
    
    // set the default data group,  if any
    if(typeof init_data !== 'undefined'){
        options.data = init_data;
    }

    // create select2 object
    obj.select2(options);

    // set the default selection, if any
    if(typeof init_data !== 'undefined'){
        var sel_ids = [];        
        $.each(init_data, function(i, o){
            sel_ids.push(o.id);
        });
        
        obj.val(sel_ids).trigger('change');
    }    
}


function formatSelect2AjaxResult(data){
    if(typeof(data) !== "undefined"){
        if(data.hint !== ""){
            return data.text + ' <span class="hint">(' + data.hint + ')</span>';
        }else{
            return data.text;
        }
    }
    return "";
}

function formatSelect2AjaxSelection(data){
    if(typeof(data) !== "undefined"){
        return data.text;
    }
    return "";
}