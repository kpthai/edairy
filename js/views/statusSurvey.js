
/**
 * 
 * 
 * @author Chanwoot  See-ngam <chanwoot2536@gmail.com>
 * @copyright 2017 chanwoot see-ngam
 * @since 1.0
 * 
 */

/**
 * Description of Farmer
 *
 * @author kla
 */
var StatusSurvey = function(){
    
    var $card_detail = $(".detail");
    var $card_training = $(".training");
    
   
    var $tb_page = $("#tb_page");
    
    var current_page = 0;
    
    var handleStatusSurvey = function(){
        $.blockUI({ message: '<h1> กำลังโหลดข้อมูล...</h1>' });
        var $coop_id = $("#coop_id");
        var $farmer_name = $("#farmer_name");
        var $start_date = $("#start_date");
        var $end_date = $("#end_date");
        var $search_btn = $("#search_btn");
    
        getCoop();
        
        //$('#startDate').datetimepicker({format: 'DD/MM/YYYY'});
        //$('#endDate').datetimepicker({format: 'DD/MM/YYYY'});

        $("#add_stat").on("click",function(){
                location.href = "add_status_survey.html";
        });
        
        
        function getCoop(){
            var params = {data: "coop",access_token: window.localStorage.getItem("access_token")};
            getJsonData(MASTER_DATA_URL, params,
                // success
                function success(data) {

                    $.each(data, function (i, item) {
                        $('select[name=coop_id]').append($('<option>', {
                            value: item.id,
                            text : item.name
                        }));
                    });

                    $(".sidebar").load("_sidebar.html");

                    $("#cow_list tbody > tr").on("click",function(){
                            window.location = "farm-detail.html";
                    });
                    $.unblockUI();
                    //getDataTable();
                },
                function error(code,desc){

                }
            );
        }

        function getDataTable(){
            var params = {access_token: window.localStorage.getItem("access_token"),farmer_name:$farmer_name.val(),coop_id:$coop_id.val(),
                            start_date:$start_date.val(),end_date:$end_date.val(),current_page:current_page};
            getJsonData(STATUS_SURVEY_URL, params,
                // success
                function success(data) {

                    
                    if(data==null){
                        $("#num_of_result").text("พบ 0 รายการ");
                        $("#cow_list tbody").html("");
                        $.unblockUI();
                        return true;
                    }
                    
                    $("#cow_list tbody").html("");
                    
                    for(var i = 0; i < data.length ; i++){
                        if(i == (data.length-1)){
                            //paging navigator
                            $("#num_of_result").html("ผลการค้นหา "+data[i].total+" รายการ");
                            
                            var num_page = Math.ceil(data[i].total/20);
                            if(num_page == 1 || num_page == 0){
                                $tb_page.html('<li class="page-item disabled">'+
                                                    '<a class="page-link" href="#" tabindex="-1">Previous</a>'+
                                                  '</li>'+
                                                  '<li class="page-item active"><a class="page-link" href="#">1</a></li>'+
                                                  '<li class="page-item disabled">'+
                                                    '<a class="page-link" href="#">Next</a>'+
                                                  '</li>');
                            }else{
                                $tb_page.html('');
                                if(current_page == 0){
                                    $tb_page.append('<li class="page-item disabled">'+
                                                    '<a class="page-link" href="#" tabindex="-1">First</a>'+
                                                  '</li>'+
                                                    '<li class="page-item disabled">'+
                                                    '<a class="page-link" href="#" tabindex="-1">Previous</a>'+
                                                  '</li>');
                                }else{
                                    $tb_page.append('<li class="page-item">'+
                                                    '<a class="page-link first" href="#" tabindex="-1">First</a>'+
                                                  '</li>'+
                                                  '<li class="page-item">'+
                                                    '<a class="page-link prev" href="#" id="'+current_page+'" tabindex="-1">Previous</a>'+
                                                  '</li>');
                                }
                                var st = 0;
                                if(current_page > 2){
                                    st = (current_page-2);
                                }
                                
                                var last = (5+st);
                                if(last>num_page){
                                    st = num_page-5;
                                    if(st < 0){
                                        st = 0;
                                    }
                                    last = num_page;
                                }
                                
                                for(var j=st;j<last;j++){
                                    if(current_page == j){
                                        $tb_page.append('<li class="page-item active"><a class="page-link" href="#">'+(j+1)+'</a></li>');
                                    }else{
                                        $tb_page.append('<li class="page-item"><a class="page-link page" href="#" id="'+j+'">'+(j+1)+'</a></li>');
                                    }
                                }
                                
                                if(current_page == (num_page-1)){
                                    $tb_page.append('<li class="page-item disabled">'+
                                                        '<a class="page-link" href="#">Next</a>'+
                                                      '</li>'+
                                                      '<li class="page-item disabled">'+
                                                        '<a class="page-link" href="#">Last</a>'+
                                                      '</li>');
                                }else{
                                    $tb_page.append('<li class="page-item">'+
                                                        '<a class="page-link next" href="#" id="'+current_page+'">Next</a>'+
                                                      '</li>'+
                                                      '<li class="page-item">'+
                                                        '<a class="page-link last" href="#" id="'+(num_page-1)+'">Last</a>'+
                                                      '</li>');
                                }
                            }
                            
                        }else{
                            $("#cow_list tbody").append('\
                                <tr>\
                                    <td>'+data[i].id+'</td>\
                                    <td>'+data[i].survey_date+'</td>\
                                    <td>'+data[i].farm_name+'</td>\
                                    <td>'+data[i].name+'</td>\
                                    <td><a href="add_status_survey.html?id='+data[i].id+'&date='+data[i].survey_date+'"><i class="fa fa-edit"></i></a></td>\
                                </tr>\
                            ');
                        }
                    }
                    
                    $.unblockUI();
                },
                function error(code,desc){

                }
            );
        }
        
        $("ul").delegate("a.prev","click",function(){
            $.blockUI({ message: '<h1> กำลังโหลดข้อมูล...</h1>' });
            var page = $(this).attr("id");
            if (page >= 1) {
                current_page--;
                getDataTable();
            }
            
        });
        
        $("ul").delegate("a.next","click",function(){
            $.blockUI({ message: '<h1> กำลังโหลดข้อมูล...</h1>' });
                current_page++;
                getDataTable();
        });
        
        $("ul").delegate("a.page","click",function(){
            
            $.blockUI({ message: '<h1> กำลังโหลดข้อมูล...</h1>' });
            var page = $(this).attr("id");
            current_page = page;
            getDataTable();
        });
        
        $("ul").delegate("a.first","click",function(){
            
            $.blockUI({ message: '<h1> กำลังโหลดข้อมูล...</h1>' });
            current_page = 0;
            getDataTable();
        });
        
        $("ul").delegate("a.last","click",function(){
            
            $.blockUI({ message: '<h1> กำลังโหลดข้อมูล...</h1>' });
            var page = $(this).attr("id");
            current_page = page;
            getDataTable();
        });
        
        $search_btn.click(function(){
            $.blockUI({ message: '<h1> กำลังโหลดข้อมูล...</h1>' });
            current_page = 0;
            getDataTable();
        });
        
    }
    
    var handleStatusSurveyDetail = function(){
        
        //$('#datetimepicker1').datetimepicker({format: 'DD/MM/YYYY'});
        
        getRegion();
        getCowProject();
        getFarmerTraining();
        
        function getRegion(){
            var params = {access_token: window.localStorage.getItem("access_token")};
            getJsonData(REGION_URL, params,
                // success
                function success(data) {
                    $.each(data,function(i,e){
                        $sel_region.append($("<option>").attr('value', e.id).text(e.name));
                    });
                    getProvince();
                },
                function error(code,desc){

                }
            );
        }
        
        function getProvince(){
            var params = {access_token: window.localStorage.getItem("access_token")};
            getJsonData(PROVINCE_URL, params,
                // success
                function success(data) {
                    $.each(data,function(i,e){
                        $sel_prov.append($("<option>").attr('value', e.id).text(e.name));
                    });
                    getDistrict();
                },
                function error(code,desc){

                }
            );
        }
        
        function getDistrict(){
             var params = {access_token: window.localStorage.getItem("access_token")};
            getJsonData(DISTRICT_URL, params,
                // success
                function success(data) {
                    $.each(data,function(i,e){
                        $sel_dist.append($("<option>").attr('value', e.id).text(e.name));
                    });
                    getSubDistrict();
                },
                function error(code,desc){

                }
            );
        }
        
        function getSubDistrict(){
             var params = {access_token: window.localStorage.getItem("access_token")};
            getJsonData(SUB_DISTRICT_URL, params,
                // success
                function success(data) {
                    $.each(data,function(i,e){
                        $sel_sub_dist.append($("<option>").attr('value', e.id).text(e.name));
                    });
                    getFarmerDetail();
                    
                },
                function error(code,desc){

                }
            );
        }
        
        function getCowProject(){
             var params = {access_token: window.localStorage.getItem("access_token")};
            getJsonData(COW_PROJECT_URL, params,
                // success
                function success(data) {
                    $.each(data,function(i,e){
                        $sel_cow_project.append($("<option>").attr('value', e.id).text(e.name));
                    });
                    getFarmerDetail();
                    
                },
                function error(code,desc){

                }
            );
        }
        
        
        
        function getFarmerTraining(){
            var params = {access_token: window.localStorage.getItem("access_token"),cmd:"get_all_training"};
            getJsonData(FARMER_TRAINING_URL, params,
                // success
                function success(data) {
                    $.each(data,function(i,e){
                        $sel_farmer_training.append($("<option>").attr('value', e.id).text(e.name));
                    });
                    
                    
                },
                function error(code,desc){

                }
            );
        }
        
        function updateFarmer(){
            var user_id = getUrlParameter("id");
            var title_id = $sel_title.val();
            var fname = $fname.val();
            var lname = $lname.val();
            var edu_id = $sel_edu.val();
            var address = $address.val();
            var region_id = $sel_region.val();
            var prov_id = $sel_prov.val();
            var dist_id = $sel_dist.val();
            var sub_dist_id = $sel_sub_dist.val();
            var postcode = $postcode.val();
            var location_data = $location_farmer.val().split(",");
            var lat = location_data[0];
            var lng = location_data[1];
            var params = {access_token: window.localStorage.getItem("access_token"),cmd:"update_farmer",
                           id:user_id,title_id:title_id,fname:fname,lname:lname,edu_id:edu_id,address:address,
                            region_id:region_id,prov_id:prov_id,dist_id:dist_id,sub_dist_id:sub_dist_id,
                            postcode:postcode,lat:lat,lng:lng};
            getJsonData(PROFILE_URL, params,
                // success
                function success(data) {
                    if(data){
                        alert("แก้ไขข้อมูลเรียบร้อย");
                    }
                    
                    
                },
                function error(code,desc){

                }
            );
        }
        
        function addFarmerTraining(){
            var farmer_id = getUrlParameter("id");
            var farmer_training_id = $sel_farmer_training.val();
            var training_date = $training_date.val();
            var training_place = $training_place.val();
            var training_day = $training_day.val();
            var params = {access_token: window.localStorage.getItem("access_token"),cmd:"add_farmer_training",
                           farmer_id:farmer_id,farmer_training_id:farmer_training_id,training_date:training_date,
                            training_place:training_place,training_day:training_day};
            getJsonData(FARMER_TRAINING_URL, params,
                // success
                function success(data) {
                    console.log(data);
                    if(data){
                        alert("เพิ่มข้อมูลเรียบร้อย");
                        $card_detail.show();
                        $card_training.hide();
                    }
                    
                    
                },
                function error(code,desc){

                }
            );
        }
        
        $btn_save_farm.click(function(){
            updateFarmer();
        });
        
        $btn_save_train.click(function(){
            addFarmerTraining();
        });
        
        $add_traning_btn.click(function(){
            
            $card_detail.hide();
            $card_training.show();
            
        });
        
        $btn_back.click(function(){
            $card_detail.show();
            $card_training.hide();
        });
        
    }
    
    var handleAddStatusSurvey = function(){
        
        var user_id = getUrlParameter("id");
        
        var $survey_date = $("#survey_date");
        var $farmer_name = $("#farmer_name");
        var $btn_save_cow_status  = $("#btn_save_cow_status");
        var cow_id;
        var farm_id;
        var cow_status_id;
   
        
        getCoop();
        
        function getCoop(){
            var params = {data: "coop",access_token: window.localStorage.getItem("access_token")};
            getJsonData(MASTER_DATA_URL, params,
                // success
                function success(data) {

                    $.each(data, function (i, item) {
                        $('select[name=coop_id]').append($('<option>', {
                            value: item.id,
                            text : item.name
                        }));
                    });
                    
                    if(user_id != null){
                        getFarmerDetail();
                    }
                },
                function error(code,desc){

                }
            );
        }
        
        function getFarmerDetail(){
            
            var user_id = getUrlParameter("id");
            var date = getUrlParameter("date");
        
            var params = {access_token: window.localStorage.getItem("access_token"),id: user_id};
            getJsonData(PROFILE_URL, params,
                // success
                function success(data) {
                    console.log(data);
                    $('select[name=coop_id]').val(data[0].coop_id);
                    $survey_date.val(date);
                    getFarmer(data[0].coop_id,user_id);
                    ListCowNotVertify(data[0].farm_id);
                    farm_id = data[0].farm_id;
                    
                },
                function error(code,desc){

                }
            );
        }
        
        function getFarmer(coop_id,farmer_id){
            var params = {access_token: window.localStorage.getItem("access_token"),coop_id:coop_id};
            getJsonData(FARMER_URL, params,
                // success
                function success(data) {
                    
                    $('select[name=farmer_name]').html("<option value=''>-- กรุณาเลือก --</option>");
                    $.each(data, function (i, item) {
                        $('select[name=farmer_name]').append($('<option>', {
                            value: item.id,
                            text : item.name+" "+item.surname
                        }));
                    });
                    
                    $farmer_name.val(farmer_id);
                },
                function error(code,desc){

                }
            );
        }
        
        $('select[name=coop_id]').change(function(){
            var coop_id = $(this).val();
            getFarmer(coop_id,"");
        });
        
        $("#datetimepicker1").datetimepicker({
            format:"YYYY-MM-DD"
        });
        
        $btn_save_cow_status.click(function(){
            addFarmSurveyTransection();
        });
        
        function addFarmSurveyTransection(){
            var survey_date = $survey_date.val();
            var farmer_id = $farmer_name.val();
            
            var params = {access_token: window.localStorage.getItem("access_token"),cmd:"add_farmer_survey_transection",survey_date:survey_date,
                            farmer_id:farmer_id};
            getJsonData(STATUS_SURVEY_URL, params,
                // success
                function success(data) {
                    //data = farm_id
                    ListCowNotVertify(data);
                    
                },
                function error(code,desc){

                }
            );
        }
        
        function ListCowNotVertify(farm_id){
            var start_date = getUrlParameter("date");
            var params = {access_token: window.localStorage.getItem("access_token"),cmd:"list_cow_not_vertify",farm_id:farm_id,start_date:start_date};
            getJsonData(COW_URL, params,
                // success
                function success(data) {
                    console.log(data);
                    ListCowVertify(farm_id);
                    if(data==null){
                        $("#num_of_result").html("ผลการค้นหา 0 รายการ");
                         $("#cow_list tbody").html("");
                        return true;
                    }
                    $("#num_of_result").html("ผลการค้นหา "+data.length+" รายการ");
                    $("#cow_list tbody").html("");
                    for(var i = 0; i < data.length ; i++){
                        var img = "1.jpg";
                        if(data[i].image_url != "" && data[i].image_url != null){
                            img = data[i].image_url;
                        }
                        $("#cow_list tbody").append('\
                            <tr id="'+data[i].id+'" cow_status_id="'+data[i].cow_status_id+'">\
                                <td class="text-center">\
                                    <div class="avatar">\
                                            <img src="img/avatars/'+img+'" class="img-avatar" alt="data[i].name">\
                                        <span class="avatar-status"></span>\
                                    </div>\
                                </td>\
                                <td>\
                                    <div>'+data[i].name+'<i class="fa fa-venus"></i></div>\
                                    <div class="small text-muted">'+data[i].cow_id+'</div>\
                                </td>\
                                <td>'+data[i].cow_status+'</td>\
                                <td class="text-center"><a href="#" class="status-confirm" data-toggle="modal" data-target="#myModal"><i class="fa fa-edit"></i></a>\
                                </td>\
                            </tr>');
                    }
                    
                    
                },
                function error(code,desc){

                }
            );
            
        }
        
        function ListCowVertify(farm_id){
            var start_date = getUrlParameter("date");
            var params = {access_token: window.localStorage.getItem("access_token"),cmd:"list_cow_vertify",farm_id:farm_id,start_date:start_date};
            getJsonData(COW_URL, params,
                // success
                function success(data) {
                    console.log(data);
                    if(data==null){
                        $("#num_of_result_approve").html("ผลการค้นหา 0 รายการ");
                         $("#cow_list_approve tbody").html("");
                        return true;
                    }
                    $("#num_of_result_approve").html("ผลการค้นหา "+data.length+" รายการ");
                    $("#cow_list_approve tbody").html("");
                    for(var i = 0; i < data.length ; i++){
                        var img = "1.jpg";
                        if(data[i].image_url != "" && data[i].image_url != null){
                            img = data[i].image_url;
                        }
                        $("#cow_list_approve tbody").append('\
                            <tr id="'+data[i].id+'" cow_status_id="'+data[i].cow_status_id+'">\
                                <td class="text-center">\
                                    <div class="avatar">\
                                            <img src="img/avatars/'+img+'" class="img-avatar" alt="data[i].name">\
                                        <span class="avatar-status"></span>\
                                    </div>\
                                </td>\
                                <td>\
                                    <div>'+data[i].name+'<i class="fa fa-venus"></i></div>\
                                    <div class="small text-muted">'+data[i].cow_id+'</div>\
                                </td>\
                                <td>'+data[i].cow_status+'</td>\
                                <td class="text-center"><a href="#" class="status-confirm" data-toggle="modal" data-target="#myModal"><i class="fa fa-edit"></i></a>\
                                </td>\
                            </tr>');
                    }
                },
                function error(code,desc){

                }
            );
            
        }
        
        function addCowHasCowStatus(id){
            
            var params = {access_token: window.localStorage.getItem("access_token"),cmd:"add_cow_status",id:id,
                            cow_status_id:cow_status_id};
            getJsonData(COW_HAS_COW_STATUS_URL, params,
                // success
                function success(data) {
                    //data = farm_id
                    getFarmerDetail();
                },
                function error(code,desc){

                }
            );
            
        }
        
        
        
        
	
	
	$("#cow_list tbody").delegate("a.status-confirm","click",function(){
		cow_id = $(this).closest("tr").attr("id");
                cow_status_id = $(this).closest("tr").attr("cow_status_id");
		
	});
	/*
	$("#save_btn").on("click",function(){
		location.href = "status_survey.html";
	});
	
	$('#datetimepicker1').datetimepicker({format: 'DD/MM/YYYY'});
	*/
	$("#approve").on("click",function(){
		addCowHasCowStatus(cow_id);
                
                $('#myModal').modal('hide');
		//$("#cow_list_approve tbody").append("<tr>"+selectedElement.html()+"</tr>");
		//selectedElement.remove();
		//$("#myModal").modal("hide");
		
	});
	
	$("#reject").on("click",function(){
                var date = getUrlParameter("date");
		location.href = "status_selection.html?farm_id="+farm_id+"&date="+date+"&cow_id="+cow_id+"";
	});
	/*
		
	$(".status-confirm").last().on("click",function(){
		selectedElement = $(this).parent();
		console.log(selectedElement.html());
	});
        
        */
        //.status-confirm data-toggle="modal" data-target="#myModal"
        
        
    }
    
    var handleEditStatusSurvey = function(){
        var farm_id = getUrlParameter("farm_id");
        var cow_id = getUrlParameter("cow_id");
        var date = getUrlParameter("date");
        
        var $cow_status = $("#cow_status");
        var $status_date = $("#status_date");
        var $btn_save_cow_status_change = $("#btn_save_cow_status_change");
        
        getCowsStatus();
        
        $('#datetimepicker1').datetimepicker({format: 'YYYY-MM-DD'});
	
        $btn_save_cow_status_change.on("click",function(){
            addCowHasCowStatus(cow_id);
                
        });

        function getCowsStatus(){
            var params = {access_token: window.localStorage.getItem("access_token"),cmd:"get_all_cow_status"};
            getJsonData(COW_STATUS_URL, params,
                // success
                function success(data) {
                    $.each(data, function (i, item) {
                        $cow_status.append($('<option>', {
                                value: item.id,
                                text : item.name
                            }));
                    });
                },
                function error(code,desc){

                }
            );
        }
        
        function addCowHasCowStatus(id){
            var  cow_status_id = $cow_status.val();
            var status_date = $status_date.val();
            var params = {access_token: window.localStorage.getItem("access_token"),cmd:"add_cow_status",id:id,
                            cow_status_id:cow_status_id,date:status_date};
            getJsonData(COW_HAS_COW_STATUS_URL, params,
                // success
                function success(data) {
                    //data = farm_id
                    location.href = "add_status_survey.html?id="+farm_id+"&date="+date+"";
                },
                function error(code,desc){

                }
            );
            
        }
        
    }
    
    return {
        init: function(){
            handleStatusSurvey();
        },
        add: function(){
            handleAddStatusSurvey();
        },
        edit: function(){
            handleEditStatusSurvey();
        },
        detail: function(){
            handleStatusSurveyDetail();
        }
    }
    
}();
