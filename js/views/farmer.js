/**
 * 
 * 
 * @author Chanwoot  See-ngam <chanwoot2536@gmail.com>
 * @copyright 2017 chanwoot see-ngam
 * @since 1.0
 * 
 */

/**
 * Description of Farmer
 *
 * @author kla
 */
var Farmer = function(){
    
    var $card_detail = $(".detail");
    var $card_training = $(".training");
    
    var $tb_page = $("#tb_page");
    
    var $coop_id = $("#coop_id");
    var $farmer_name = $("#farmer_name");
    var $search_btn = $("#search_btn");
    
    var $farm_name = $("#farm_name");
    var $sel_cow_project = $("#sel_cow_project");
    var $sel_edu = $("#sel_edu");
    var $coop = $("#coop");
    var $member_code = $("#member_code");
    //var $farm_status = $("#farm_status");
    var $member_start_date = $("#member_start_date");
    
    var $fname = $("#fname");
    var $lname = $("#lname");
    var $sel_title = $("#sel_title");
    var $sel_region = $("#sel_region");
    var $sel_prov = $("#sel_prov");
    var $sel_dist = $("#sel_dist");
    var $sel_sub_dist = $("#sel_sub_dist");
    var $address = $("#address");
    var $citizen_id = $("#citizen_id");
    var $postcode = $("#postcode");
    var $location_farmer = $("#location_farmer");
    var $btn_save_farm = $("#btn_save_farm");
    var $btn_get_location = $("#btn_get_location");
    //อบรม
    var $add_traning_btn = $("#add_traning_btn");
    var $training_id = $("#training_id");
    var $sel_farmer_training = $("#sel_farmer_training");
    var $training_date = $("#training_date");
    var $training_place = $("#training_place");
    var $training_day = $("#training_day");
    
    var $btn_save_train = $("#btn_save_train");
    var $btn_back = $("#btn_back");
    
    var current_page = 0;
    
    var handleFarmer = function(){
        $.blockUI({ message: '<h1> กำลังโหลดข้อมูล...</h1>' });
        
        var role_id = window.localStorage.getItem("role");
        if(role_id == 1){
            enableObject($("input"),false);
            enableObject($("select"),false);
        }else{
            enableObject($("input"),true);
            enableObject($("select"),true);
        }
        getCoop();
        
        function getCoop(){
            var params = {data: "coop",access_token: window.localStorage.getItem("access_token")};
            getJsonData(MASTER_DATA_URL, params,
                // success
                function success(data) {

                    $.each(data, function (i, item) {
                        $('select[name=coop_id]').append($('<option>', {
                            value: item.id,
                            text : item.name
                        }));
                    });

                    $(".sidebar").load("_sidebar.html");

                    $("#cow_list tbody > tr").on("click",function(){
                            window.location = "farm-detail.html";
                    });
                    $.unblockUI();
                    //getDataTable();
                },
                function error(code,desc){

                }
            );
        }

        function getDataTable(){
            var params = {access_token: window.localStorage.getItem("access_token"),farmer_name:$farmer_name.val(),coop_id:$coop_id.val(),current_page:current_page};
            getJsonData(FARMER_URL, params,
                // success
                function success(data) {

                    if(data==null){
                        $("#num_of_result").html("ผลการค้นหา 0 รายการ");
                        $("#farmer_list tbody").html("");
                        $.unblockUI();
                        return true;
                    }
                    
                    $("#farmer_list tbody").html("");
                    for(var i = 0; i < data.length ; i++){
                        if(i == (data.length-1)){
                            //paging navigator
                            $("#num_of_result").html("ผลการค้นหา "+data[i].total+" รายการ");
                            
                            var num_page = Math.ceil(data[i].total/20);
                            if(num_page == 1 || num_page == 0){
                                $tb_page.html('<li class="page-item disabled">'+
                                                    '<a class="page-link" href="#" tabindex="-1">Previous</a>'+
                                                  '</li>'+
                                                  '<li class="page-item active"><a class="page-link" href="#">1</a></li>'+
                                                  '<li class="page-item disabled">'+
                                                    '<a class="page-link" href="#">Next</a>'+
                                                  '</li>');
                            }else{
                                $tb_page.html('');
                                if(current_page == 0){
                                    $tb_page.append('<li class="page-item disabled">'+
                                                    '<a class="page-link" href="#" tabindex="-1">First</a>'+
                                                  '</li>'+
                                                    '<li class="page-item disabled">'+
                                                    '<a class="page-link" href="#" tabindex="-1">Previous</a>'+
                                                  '</li>');
                                }else{
                                    $tb_page.append('<li class="page-item">'+
                                                    '<a class="page-link first" href="#" tabindex="-1">First</a>'+
                                                  '</li>'+
                                                  '<li class="page-item">'+
                                                    '<a class="page-link prev" href="#" id="'+current_page+'" tabindex="-1">Previous</a>'+
                                                  '</li>');
                                }
                                var st = 0;
                                if(current_page > 2){
                                    st = (current_page-2);
                                }
                                
                                var last = (5+st);
                                if(last>num_page){
                                    st = num_page-5;
                                    if(st < 0){
                                        st = 0;
                                    }
                                    last = num_page;
                                }
                                
                                for(var j=st;j<last;j++){
                                    if(current_page == j){
                                        $tb_page.append('<li class="page-item active"><a class="page-link" href="#">'+(j+1)+'</a></li>');
                                    }else{
                                        $tb_page.append('<li class="page-item"><a class="page-link page" href="#" id="'+j+'">'+(j+1)+'</a></li>');
                                    }
                                }
                                
                                if(current_page == (num_page-1)){
                                    $tb_page.append('<li class="page-item disabled">'+
                                                        '<a class="page-link" href="#">Next</a>'+
                                                      '</li>'+
                                                      '<li class="page-item disabled">'+
                                                        '<a class="page-link" href="#">Last</a>'+
                                                      '</li>');
                                }else{
                                    $tb_page.append('<li class="page-item">'+
                                                        '<a class="page-link next" href="#" id="'+current_page+'">Next</a>'+
                                                      '</li>'+
                                                      '<li class="page-item">'+
                                                        '<a class="page-link last" href="#" id="'+(num_page-1)+'">Last</a>'+
                                                      '</li>');
                                }
                            }
                            
                        }else{
                        $("#farmer_list tbody").append('\
                                <tr>\
                                    <td>'+data[i].member_code+'</td>\
                                    <td>'+data[i].name+' '+data[i].surname+'</td>\
                                    <td><a href="farm-detail.html?id='+data[i].id+'"><i class="fa fa-edit"></i></a></td>\
                                </tr>');
                        }
                    }
                    
                    $.unblockUI();
                    
                },
                function error(code,desc){

                }
            );
        }
        
        $("ul").delegate("a.prev","click",function(){
            $.blockUI({ message: '<h1> กำลังโหลดข้อมูล...</h1>' });
            var page = $(this).attr("id");
            if (page >= 1) {
                current_page--;
                getDataTable();
            }
            
        });
        
        $("ul").delegate("a.next","click",function(){
            $.blockUI({ message: '<h1> กำลังโหลดข้อมูล...</h1>' });
                current_page++;
                getDataTable();
        });
        
        $("ul").delegate("a.page","click",function(){
            
            $.blockUI({ message: '<h1> กำลังโหลดข้อมูล...</h1>' });
            var page = $(this).attr("id");
            current_page = page;
            getDataTable();
        });
        
        $("ul").delegate("a.first","click",function(){
            
            $.blockUI({ message: '<h1> กำลังโหลดข้อมูล...</h1>' });
            current_page = 0;
            getDataTable();
        });
        
        $("ul").delegate("a.last","click",function(){
            
            $.blockUI({ message: '<h1> กำลังโหลดข้อมูล...</h1>' });
            var page = $(this).attr("id");
            current_page = page;
            getDataTable();
        });
        
        
        
        
        
        
        
        $search_btn.click(function(){
            $.blockUI({ message: '<h1> กำลังโหลดข้อมูล...</h1>' });
            current_page = 0;
            getDataTable();
        });
        
    }
    
    var handleFarmerDetail = function(){
        $.blockUI({ message: '<h1> กำลังโหลดข้อมูล...</h1>' });
        
        //$(document).ajaxStart($.blockUI).ajaxStop($.unblockUI);
        var role_id = window.localStorage.getItem("role");
        if(role_id == 1){
            enableObject($("input"),false);
            enableObject($("select"),false);
        }else{
            enableObject($("input"),true);
            enableObject($("select"),true);
        }
        
        var cmd_action = "add";
        //$('#datetimepicker1').datetimepicker({format: 'DD/MM/YYYY'});
        
        getRegion();
        getCowProject();
        getFarmerTraining();
        
        $("#datetimepicker1").datetimepicker({
            format:"YYYY-MM-DD"
        });
        
        $("#datetimepicker2").datetimepicker({
            format:"YYYY-MM-DD"
        });
        
        function getRegion(){
            var params = {access_token: window.localStorage.getItem("access_token")};
            getJsonData(REGION_URL, params,
                // success
                function success(data) {
                    $.each(data,function(i,e){
                        $sel_region.append($("<option>").attr('value', e.id).text(e.name));
                    });
                    getProvince();
                },
                function error(code,desc){

                }
            );
        }
        
        function getProvince(){
            var params = {access_token: window.localStorage.getItem("access_token")};
            getJsonData(PROVINCE_URL, params,
                // success
                function success(data) {
                    $.each(data,function(i,e){
                        $sel_prov.append($("<option>").attr('value', e.id).text(e.name));
                    });
                    getDistrict();
                },
                function error(code,desc){

                }
            );
        }
        
        function getDistrict(){
             var params = {access_token: window.localStorage.getItem("access_token")};
            getJsonData(DISTRICT_URL, params,
                // success
                function success(data) {
                    $.each(data,function(i,e){
                        $sel_dist.append($("<option>").attr('value', e.id).text(e.name));
                    });
                    getSubDistrict();
                },
                function error(code,desc){

                }
            );
        }
        
        function getSubDistrict(){
             var params = {access_token: window.localStorage.getItem("access_token")};
            getJsonData(SUB_DISTRICT_URL, params,
                // success
                function success(data) {
                    $.each(data,function(i,e){
                        $sel_sub_dist.append($("<option>").attr('value', e.id).text(e.name));
                    });
                    getFarmerDetail();
                    
                },
                function error(code,desc){

                }
            );
        }
        
        function getCowProject(){
             var params = {access_token: window.localStorage.getItem("access_token")};
            getJsonData(COW_PROJECT_URL, params,
                // success
                function success(data) {
                    $.each(data,function(i,e){
                        $sel_cow_project.append($("<option>").attr('value', e.id).text(e.name));
                    });
                    getFarmerDetail();
                    
                },
                function error(code,desc){

                }
            );
        }
        
        function getFarmerDetail(){
            
            var user_id = getUrlParameter("id");
        
            var params = {access_token: window.localStorage.getItem("access_token"),cmd:"get_farmer_info",id: user_id};
            getJsonData(PROFILE_URL, params,
                // success
                function success(data) {
                    
                    $farm_name.text(data.farm_name);
                    $coop.text(data.coop_name);
                    $member_code.text(data.member_code);
                    $sel_cow_project.val(data.farm_project_id);
                    var status =  data.status;
                    if(status != ""){
                        $("input[name=farm_status][value=" + data.status + "]").attr('checked', 'checked');
                    }
                    $member_start_date.val(data.member_start_date);
                    
                    $fname.val(data.farmer_name);
                    $lname.val(data.farmer_surname);
                    $sel_title.val(data.title_id);
                    $sel_edu.val(data.education_level_id);
                    //$("textarea[name=address]").val(data.address);
                    //$("input[name=mobile]").val(data.mobile);
                    $sel_region.val(data.region_id);
                    $sel_prov.val(data.province_id);
                    $sel_dist.val(data.district_id);
                    $sel_sub_dist.val(data.sub_district_id);
                    $address.val(data.address);
                    $citizen_id.text(data.citizen_id);
                    $postcode.val(data.post_code);
                    $sel_cow_project.val(data.farm_project_id);
                    $location_farmer.val(data.latitude+","+data.longitude);
                    
                    getFarmerTraningTable();
                },
                function error(code,desc){

                }
            );
        }
        
        function getFarmerTraningTable(){
            var user_id = getUrlParameter("id");
            var params = {access_token: window.localStorage.getItem("access_token"),cmd:"get_training_table",farmer_id:user_id};
            getJsonData(FARMER_TRAINING_URL, params,
                // success
                function success(data) {

                    if(data==null){
                        $("#training_list tbody").html("");
                        $.unblockUI();
                        return true;
                    }
                    $("#training_list tbody").html("");
                    for(var i = 0; i < data.length ; i++){
                        $("#training_list tbody").append('\
                                <tr id="'+data[i].trainingId+'">\
                                    <td>'+data[i].trainingDate+'</td>\
                                    <td>'+data[i].programName+'/'+data[i].trainingPlace+'/'+data[i].trainingDay+'</td>\
                                    <td><a href="#" class="edit-training"><i class="fa fa-edit"></i></a></td>\
                                </tr>');
                    }
                    
                    $.unblockUI();
                },
                function error(code,desc){

                }
            );
        }
        
        function getLocation() {
            $.blockUI({ message: '<h1> กำลังโหลดตำแหน่ง...</h1>' });
            if (navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(showPosition);
            } else { 
                alert("Geolocation is not supported.");
            }
        }
        
        function showPosition(position) {
            $location_farmer.val(position.coords.latitude+","+position.coords.longitude);
            $.unblockUI();
        }
        
        $("#training_list").delegate("a.edit-training","click",function(){
            cmd_action = "edit";
            var $row = $(this).closest('tr');
            var id = $row.attr("id");
            getFarmerTrainingInfo(id);
        });
        
        function getFarmerTrainingInfo(id){
            var params = {access_token: window.localStorage.getItem("access_token"),cmd:"get_training_info",id:id};
            getJsonData(FARMER_TRAINING_URL, params,
                // success
                function success(data) {
                    $training_id.val(id);
                    $sel_farmer_training.val(data.training_program_id);
                    $training_date.val(data.training_date);
                    $training_place.val(data.training_place);
                    $training_day.val(data.training_day);
                    
                    $card_detail.hide();
                    $card_training.show();

                },
                function error(code,desc){

                }
            );
        }
        
        function getFarmerTraining(){
            var params = {access_token: window.localStorage.getItem("access_token"),cmd:"get_all_training"};
            getJsonData(FARMER_TRAINING_URL, params,
                // success
                function success(data) {
                    $.each(data,function(i,e){
                        $sel_farmer_training.append($("<option>").attr('value', e.id).text(e.name));
                    });
                    
                    
                },
                function error(code,desc){

                }
            );
        }
        
        function updateFarmer(){
            var user_id = getUrlParameter("id");
            var title_id = $sel_title.val();
            var fname = $fname.val();
            var lname = $lname.val();
            var edu_id = $sel_edu.val();
            var address = $address.val();
            var region_id = $sel_region.val();
            var prov_id = $sel_prov.val();
            var dist_id = $sel_dist.val();
            var sub_dist_id = $sel_sub_dist.val();
            var postcode = $postcode.val();
            var location_data = $location_farmer.val().split(",");
            var lat = location_data[0];
            var lng = location_data[1];
            var params = {access_token: window.localStorage.getItem("access_token"),cmd:"update_farmer",
                           id:user_id,title_id:title_id,fname:fname,lname:lname,edu_id:edu_id,address:address,
                            region_id:region_id,prov_id:prov_id,dist_id:dist_id,sub_dist_id:sub_dist_id,
                            postcode:postcode,lat:lat,lng:lng};
            getJsonData(PROFILE_URL, params,
                // success
                function success(data) {
                    if(data){
                        alert("แก้ไขข้อมูลเรียบร้อย");
                    }
                    
                    
                },
                function error(code,desc){

                }
            );
        }
        
        function addFarmerTraining(){
            var farmer_id = getUrlParameter("id");
            var farmer_training_id = $sel_farmer_training.val();
            var training_date = $training_date.val();
            var training_place = $training_place.val();
            var training_day = $training_day.val();
            var params = {access_token: window.localStorage.getItem("access_token"),cmd:"add_farmer_training",
                           farmer_id:farmer_id,farmer_training_id:farmer_training_id,training_date:training_date,
                            training_place:training_place,training_day:training_day};
            getJsonData(FARMER_TRAINING_URL, params,
                // success
                function success(data) {
                    console.log(data);
                    if(data){
                        alert("เพิ่มข้อมูลเรียบร้อย");
                        $card_detail.show();
                        $card_training.hide();
                    }
                    
                    
                },
                function error(code,desc){

                }
            );
        }
        
        function updateFarmerTraining(){
            var id = $training_id.val();
            var farmer_training_id = $sel_farmer_training.val();
            var training_date = $training_date.val();
            var training_place = $training_place.val();
            var training_day = $training_day.val();
            var params = {access_token: window.localStorage.getItem("access_token"),cmd:"update_farmer_training",
                           id:id,farmer_training_id:farmer_training_id,training_date:training_date,
                            training_place:training_place,training_day:training_day};
            getJsonData(FARMER_TRAINING_URL, params,
                // success
                function success(data) {
                    console.log(data);
                    if(data){
                        alert("แก้ไขข้อมูลเรียบร้อย");
                        $card_detail.show();
                        $card_training.hide();
                    }
                    
                    
                },
                function error(code,desc){

                }
            );
        }
        
        $btn_get_location.click(function(){
            getLocation();
        });
        
        $btn_save_farm.click(function(){
            updateFarmer();
        });
        
        $btn_save_train.click(function(){
            if(cmd_action == "add"){
                addFarmerTraining();
            }else{
                updateFarmerTraining();
            }
        });
        
        $add_traning_btn.click(function(){
            cmd_action = "add";
            $card_detail.hide();
            $card_training.show();
            
        });
        
        $btn_back.click(function(){
            $card_detail.show();
            $card_training.hide();
        });
        
    }
    
    return {
        init: function(){
            handleFarmer();
        },
        detail: function(){
            handleFarmerDetail();
        }
    }
    
}();
