
/**
 * 
 * 
 * @author Chanwoot  See-ngam <chanwoot2536@gmail.com>
 * @copyright 2017 chanwoot see-ngam
 * @since 1.0
 * 
 */

/**
 * Description of Farmer
 *
 * @author kla
 */
var Birth = function(){
    
    var $card_detail = $(".detail");
    var $card_training = $(".training");
    
   
    var $tb_page = $("#tb_page");
    
    var current_page = 0;
    
    var handleBirth = function(){
        $.blockUI({ message: '<h1> กำลังโหลดข้อมูล...</h1>' });
        var $coop_id = $("#coop_id");
        var $farmer_name = $("#farmer_name");
        var $search_btn = $("#search_btn");
    
        getCoop();
        
        //$('#startDate').datetimepicker({format: 'DD/MM/YYYY'});
        //$('#endDate').datetimepicker({format: 'DD/MM/YYYY'});

        $("#add_breedind").on("click",function(){
                location.href = "add_breeding_service.html";
        });
        
        $('#startDate').datetimepicker({format: 'DD/MM/YYYY'});
        $('#endDate').datetimepicker({format: 'DD/MM/YYYY'});
        
        function getCoop(){
            var params = {data: "coop",access_token: window.localStorage.getItem("access_token")};
            getJsonData(MASTER_DATA_URL, params,
                // success
                function success(data) {

                    $.each(data, function (i, item) {
                        $('select[name=coop_id]').append($('<option>', {
                            value: item.id,
                            text : item.name
                        }));
                    });

                    $(".sidebar").load("_sidebar.html");

                    $("#cow_list tbody > tr").on("click",function(){
                            window.location = "farm-detail.html";
                    });
                    $.unblockUI();
                    //getDataTable();
                },
                function error(code,desc){

                }
            );
        }

        function getDataTable(){
            var params = {access_token: window.localStorage.getItem("access_token"),farmer_name:$farmer_name.val(),coop_id:$coop_id.val()};
            getJsonData(BIRTH_URL, params,
                // success
                function success(data) {

                    if(data==null){
                        $("#num_of_result").text("พบ 0 รายการ");
                        $("#cow_list tbody").html("");
                        $.unblockUI();
                        return true;
                    }
                    
                    $("#cow_list tbody").html("");
                    
                    
                    for(var i = 0; i < data.length ; i++){
                        if(i == (data.length-1)){
                            //paging navigator
                            $("#num_of_result").html("ผลการค้นหา "+data[i].total+" รายการ");
                            
                            var num_page = Math.ceil(data[i].total/20);
                            if(num_page == 1 || num_page == 0){
                                $tb_page.html('<li class="page-item disabled">'+
                                                    '<a class="page-link" href="#" tabindex="-1">Previous</a>'+
                                                  '</li>'+
                                                  '<li class="page-item active"><a class="page-link" href="#">1</a></li>'+
                                                  '<li class="page-item disabled">'+
                                                    '<a class="page-link" href="#">Next</a>'+
                                                  '</li>');
                            }else{
                                $tb_page.html('');
                                if(current_page == 0){
                                    $tb_page.append('<li class="page-item disabled">'+
                                                    '<a class="page-link" href="#" tabindex="-1">First</a>'+
                                                  '</li>'+
                                                    '<li class="page-item disabled">'+
                                                    '<a class="page-link" href="#" tabindex="-1">Previous</a>'+
                                                  '</li>');
                                }else{
                                    $tb_page.append('<li class="page-item">'+
                                                    '<a class="page-link first" href="#" tabindex="-1">First</a>'+
                                                  '</li>'+
                                                  '<li class="page-item">'+
                                                    '<a class="page-link prev" href="#" id="'+current_page+'" tabindex="-1">Previous</a>'+
                                                  '</li>');
                                }
                                var st = 0;
                                if(current_page > 2){
                                    st = (current_page-2);
                                }
                                
                                var last = (5+st);
                                if(last>num_page){
                                    st = num_page-5;
                                    if(st < 0){
                                        st = 0;
                                    }
                                    last = num_page;
                                }
                                
                                for(var j=st;j<last;j++){
                                    if(current_page == j){
                                        $tb_page.append('<li class="page-item active"><a class="page-link" href="#">'+(j+1)+'</a></li>');
                                    }else{
                                        $tb_page.append('<li class="page-item"><a class="page-link page" href="#" id="'+j+'">'+(j+1)+'</a></li>');
                                    }
                                }
                                
                                if(current_page == (num_page-1)){
                                    $tb_page.append('<li class="page-item disabled">'+
                                                        '<a class="page-link" href="#">Next</a>'+
                                                      '</li>'+
                                                      '<li class="page-item disabled">'+
                                                        '<a class="page-link" href="#">Last</a>'+
                                                      '</li>');
                                }else{
                                    $tb_page.append('<li class="page-item">'+
                                                        '<a class="page-link next" href="#" id="'+current_page+'">Next</a>'+
                                                      '</li>'+
                                                      '<li class="page-item">'+
                                                        '<a class="page-link last" href="#" id="'+(num_page-1)+'">Last</a>'+
                                                      '</li>');
                                }
                            }
                            
                        }else{
                            
                           $("#cow_list tbody").append('\
        						<tr>\
        							<td id="result_detail">\
        								<div>'+data[i].id+' '+data[i].cowName+'</div>\
        								<div>กำหนดวันคลอด</div>\
        							</td>\
        							<td id="result_date">\
        								<div class="col-12" style="text-align:center;">วันที่</div>\
        								<div >'+data[i].create_date+'</div>\
        							</td>\
        							<td><a href="birth_datail.html?id='+data[i].cbiId+'"><i class="fa fa-edit"></i></a></td>\
        						</tr>\
        				');
                        }
                    }
                    
                    $.unblockUI();
                    
                },
                function error(code,desc){

                }
            );
        }
        
        $("ul").delegate("a.prev","click",function(){
            $.blockUI({ message: '<h1> กำลังโหลดข้อมูล...</h1>' });
            var page = $(this).attr("id");
            if (page >= 1) {
                current_page--;
                getDataTable();
            }
            
        });
        
        $("ul").delegate("a.next","click",function(){
            $.blockUI({ message: '<h1> กำลังโหลดข้อมูล...</h1>' });
                current_page++;
                getDataTable();
        });
        
        $("ul").delegate("a.page","click",function(){
            
            $.blockUI({ message: '<h1> กำลังโหลดข้อมูล...</h1>' });
            var page = $(this).attr("id");
            current_page = page;
            getDataTable();
        });
        
        $("ul").delegate("a.first","click",function(){
            
            $.blockUI({ message: '<h1> กำลังโหลดข้อมูล...</h1>' });
            current_page = 0;
            getDataTable();
        });
        
        $("ul").delegate("a.last","click",function(){
            
            $.blockUI({ message: '<h1> กำลังโหลดข้อมูล...</h1>' });
            var page = $(this).attr("id");
            current_page = page;
            getDataTable();
        });
        
        $search_btn.click(function(){
            $.blockUI({ message: '<h1> กำลังโหลดข้อมูล...</h1>' });
            current_page = 0;
            getDataTable();
        });
        
    }
    
    
    var handleAddBirth = function(){
        var id = getUrlParameter("id");
        
        var $cow_data = $("#cow_data");
        var $check_date = $("#check_date");
        var $child_weight = $("#child_weight");
        
        $('#datetimepicker1').datetimepicker({format: 'YYYY-MM-DD'});

        
        getBirthDetail();
        
        function getBirthDetail(){
        
            var params = {access_token: window.localStorage.getItem("access_token"),cmd:"get_birth_detail",id: id};
            getJsonData(BIRTH_URL, params,
                // success
                function success(data) {
                    console.log(data);
                    //getCow(data[0].farm_id,"");
                    $cow_data.text(data.cow_id+" "+data.cowName);
                    if(data.birth_date == "0000-00-00"){
                        $check_date.val("");
                    }else{
                        $check_date.val(data.birth_date);
                    }
                    $child_weight.val(data.child_weight);
                    $("input[name=birth_method][value=" + data.birth_method + "]").attr('checked', 'checked');
                    $("input[name=is_child_alive][value=" + data.is_child_alive + "]").attr('checked', 'checked');
                    $("input[name=child_gender][value=" + data.child_gender + "]").attr('checked', 'checked');
                    $("input[name=child_cow_id][value=" + data.child_cow_id + "]").attr('checked', 'checked');
                    
                    
                },
                function error(code,desc){

                }
            );
        }
        
        
        function updateBirth(){
            
            var check_date = $check_date.val();
            var child_weight = $child_weight.val();
            var birth_method = $('input[name=birth_method]:checked').val();
            var is_child_alive = $('input[name=is_child_alive]:checked').val();
            var child_gender = $('input[name=child_gender]:checked').val();
            var child_cow_id = $('input[name=child_cow_id]:checked').val();
            
            var params = {access_token: window.localStorage.getItem("access_token"),cmd:"edit_birth",
                            id:id,check_date:check_date,child_weight:child_weight,birth_method:birth_method,
                            is_child_alive:is_child_alive,child_gender:child_gender,child_cow_id:child_cow_id};
            getJsonData(BIRTH_URL, params,
                // success
                function success(data) {
                    if(child_cow_id == "1"){
                        location.href = "cattle_detail.html";
                    }else{
                        location.href = "birth.html";
                    }
            
                },
                function error(code,desc){

                }
            );
    
        }
        
        $("#submit_btn").on("click",function(){
            updateBirth();
        });

        
    }
    
    
    
    return {
        init: function(){
            handleBirth();
        },
        add: function(){
            handleAddBirth();
        }
    }
    
}();
