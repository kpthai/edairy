$(function (){
  'use strict';



  var randomScalingFactor = function(){ return Math.round(Math.random()*100)};
  var barChartData = {
    labels : ['January','February','March','April','May','June','July'],
    datasets : [
      {
        label: 'ค่าใช้จ่าย (บาท)',
        backgroundColor : 'rgba(151,187,205,0.5)',
        borderColor : 'rgba(151,187,205,0.8)',
        highlightFill : 'rgba(151,187,205,0.75)',
        highlightStroke : 'rgba(151,187,205,1)',
        data : [randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor()]
      }
    ]
  }
  var ctx = document.getElementById('canvas-2');
  var chart = new Chart(ctx, {
    type: 'bar',
    data: barChartData,
    options: {
      responsive: true
    }
  });

});
