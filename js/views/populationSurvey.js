
/**
 * 
 * 
 * @author Chanwoot  See-ngam <chanwoot2536@gmail.com>
 * @copyright 2017 chanwoot see-ngam
 * @since 1.0
 * 
 */

/**
 * Description of Farmer
 *
 * @author kla
 */
var PopulationSurvey = function(){
    
    var $card_detail = $(".detail");
    var $card_training = $(".training");
    
   
    var $tb_page = $("#tb_page");
    
    var current_page = 0;
    
    var handlePopulationSurvey = function(){
        $.blockUI({ message: '<h1> กำลังโหลดข้อมูล...</h1>' });
        var $coop_id = $("#coop_id");
        var $farmer_name = $("#farmer_name");
        var $start_date = $("#start_date");
        var $end_date = $("#end_date");
        var $search_btn = $("#search_btn");
    
        getCoop();
        
        //$('#startDate').datetimepicker({format: 'DD/MM/YYYY'});
        //$('#endDate').datetimepicker({format: 'DD/MM/YYYY'});

        $("#add_population").on("click",function(){
                location.href = "add_population_survey.html";
        });
        
        
        function getCoop(){
            var params = {data: "coop",access_token: window.localStorage.getItem("access_token")};
            getJsonData(MASTER_DATA_URL, params,
                // success
                function success(data) {

                    $.each(data, function (i, item) {
                        $('select[name=coop_id]').append($('<option>', {
                            value: item.id,
                            text : item.name
                        }));
                    });

                    $(".sidebar").load("_sidebar.html");

                    $("#cow_list tbody > tr").on("click",function(){
                            window.location = "farm-detail.html";
                    });
                    $.unblockUI();
                    //getDataTable();
                },
                function error(code,desc){

                }
            );
        }

        function getDataTable(){
            var params = {access_token: window.localStorage.getItem("access_token"),farmer_name:$farmer_name.val(),coop_id:$coop_id.val(),
                            start_date:$start_date.val(),end_date:$end_date.val(),current_page:current_page};
            getJsonData(POPULATION_SURVEY_URL, params,
                // success
                function success(data) {

                    //console.log(data);
                    if(data==null){
                        $("#num_of_result").text("พบ 0 รายการ");
                        $("#cow_list tbody").html("");
                        $.unblockUI();
                        return true;
                    }
                    $("#cow_list tbody").html("");
                    
                    for(var i = 0; i < data.length ; i++){
                        if(i == (data.length-1)){
                            //paging navigator
                            $("#num_of_result").html("ผลการค้นหา "+data[i].total+" รายการ");
                            
                            var num_page = Math.ceil(data[i].total/20);
                            if(num_page == 1 || num_page == 0){
                                $tb_page.html('<li class="page-item disabled">'+
                                                    '<a class="page-link" href="#" tabindex="-1">Previous</a>'+
                                                  '</li>'+
                                                  '<li class="page-item active"><a class="page-link" href="#">1</a></li>'+
                                                  '<li class="page-item disabled">'+
                                                    '<a class="page-link" href="#">Next</a>'+
                                                  '</li>');
                            }else{
                                $tb_page.html('');
                                if(current_page == 0){
                                    $tb_page.append('<li class="page-item disabled">'+
                                                    '<a class="page-link" href="#" tabindex="-1">First</a>'+
                                                  '</li>'+
                                                    '<li class="page-item disabled">'+
                                                    '<a class="page-link" href="#" tabindex="-1">Previous</a>'+
                                                  '</li>');
                                }else{
                                    $tb_page.append('<li class="page-item">'+
                                                    '<a class="page-link first" href="#" tabindex="-1">First</a>'+
                                                  '</li>'+
                                                  '<li class="page-item">'+
                                                    '<a class="page-link prev" href="#" id="'+current_page+'" tabindex="-1">Previous</a>'+
                                                  '</li>');
                                }
                                var st = 0;
                                if(current_page > 2){
                                    st = (current_page-2);
                                }
                                
                                var last = (5+st);
                                if(last>num_page){
                                    st = num_page-5;
                                    if(st < 0){
                                        st = 0;
                                    }
                                    last = num_page;
                                }
                                
                                for(var j=st;j<last;j++){
                                    if(current_page == j){
                                        $tb_page.append('<li class="page-item active"><a class="page-link" href="#">'+(j+1)+'</a></li>');
                                    }else{
                                        $tb_page.append('<li class="page-item"><a class="page-link page" href="#" id="'+j+'">'+(j+1)+'</a></li>');
                                    }
                                }
                                
                                if(current_page == (num_page-1)){
                                    $tb_page.append('<li class="page-item disabled">'+
                                                        '<a class="page-link" href="#">Next</a>'+
                                                      '</li>'+
                                                      '<li class="page-item disabled">'+
                                                        '<a class="page-link" href="#">Last</a>'+
                                                      '</li>');
                                }else{
                                    $tb_page.append('<li class="page-item">'+
                                                        '<a class="page-link next" href="#" id="'+current_page+'">Next</a>'+
                                                      '</li>'+
                                                      '<li class="page-item">'+
                                                        '<a class="page-link last" href="#" id="'+(num_page-1)+'">Last</a>'+
                                                      '</li>');
                                }
                            }
                            
                        }else{
                            $("#cow_list tbody").append('\
                                            <tr>\
                                                <td>'+data[i].id+'</td>\
                                                <td>'+data[i].create_date+'</td>\
                                                <td>'+data[i].name+'</td>\
                                                <td>'+data[i].total_cow+'</td>\
                                                <td><a href="add_population_survey.html?id='+data[i].id+'"><i class="fa fa-edit"></i></a></td>\
                                            </tr>\
                            ');
                        }
                    }
                    
                    
                    $.unblockUI();
                },
                function error(code,desc){

                }
            );
        }
        
        $("ul").delegate("a.prev","click",function(){
            $.blockUI({ message: '<h1> กำลังโหลดข้อมูล...</h1>' });
            var page = $(this).attr("id");
            if (page >= 1) {
                current_page--;
                getDataTable();
            }
            
        });
        
        $("ul").delegate("a.next","click",function(){
            $.blockUI({ message: '<h1> กำลังโหลดข้อมูล...</h1>' });
                current_page++;
                getDataTable();
        });
        
        $("ul").delegate("a.page","click",function(){
            
            $.blockUI({ message: '<h1> กำลังโหลดข้อมูล...</h1>' });
            var page = $(this).attr("id");
            current_page = page;
            getDataTable();
        });
        
        $("ul").delegate("a.first","click",function(){
            
            $.blockUI({ message: '<h1> กำลังโหลดข้อมูล...</h1>' });
            current_page = 0;
            getDataTable();
        });
        
        $("ul").delegate("a.last","click",function(){
            
            $.blockUI({ message: '<h1> กำลังโหลดข้อมูล...</h1>' });
            var page = $(this).attr("id");
            current_page = page;
            getDataTable();
        });
        
        $search_btn.click(function(){
            $.blockUI({ message: '<h1> กำลังโหลดข้อมูล...</h1>' });
            current_page = 0;
            getDataTable();
        });
        
    }
    
    var handlePopulationSurveyDetail = function(){
        
        //$('#datetimepicker1').datetimepicker({format: 'DD/MM/YYYY'});
        
        getRegion();
        getCowProject();
        getFarmerTraining();
        
        function getRegion(){
            var params = {access_token: window.localStorage.getItem("access_token")};
            getJsonData(REGION_URL, params,
                // success
                function success(data) {
                    $.each(data,function(i,e){
                        $sel_region.append($("<option>").attr('value', e.id).text(e.name));
                    });
                    getProvince();
                },
                function error(code,desc){

                }
            );
        }
        
        function getProvince(){
            var params = {access_token: window.localStorage.getItem("access_token")};
            getJsonData(PROVINCE_URL, params,
                // success
                function success(data) {
                    $.each(data,function(i,e){
                        $sel_prov.append($("<option>").attr('value', e.id).text(e.name));
                    });
                    getDistrict();
                },
                function error(code,desc){

                }
            );
        }
        
        function getDistrict(){
             var params = {access_token: window.localStorage.getItem("access_token")};
            getJsonData(DISTRICT_URL, params,
                // success
                function success(data) {
                    $.each(data,function(i,e){
                        $sel_dist.append($("<option>").attr('value', e.id).text(e.name));
                    });
                    getSubDistrict();
                },
                function error(code,desc){

                }
            );
        }
        
        function getSubDistrict(){
             var params = {access_token: window.localStorage.getItem("access_token")};
            getJsonData(SUB_DISTRICT_URL, params,
                // success
                function success(data) {
                    $.each(data,function(i,e){
                        $sel_sub_dist.append($("<option>").attr('value', e.id).text(e.name));
                    });
                    getFarmerDetail();
                    
                },
                function error(code,desc){

                }
            );
        }
        
        function getCowProject(){
             var params = {access_token: window.localStorage.getItem("access_token")};
            getJsonData(COW_PROJECT_URL, params,
                // success
                function success(data) {
                    $.each(data,function(i,e){
                        $sel_cow_project.append($("<option>").attr('value', e.id).text(e.name));
                    });
                    getFarmerDetail();
                    
                },
                function error(code,desc){

                }
            );
        }
        
        function getFarmerDetail(){
            
            var user_id = getUrlParameter("id");
        
            var params = {access_token: window.localStorage.getItem("access_token"),id: user_id};
            getJsonData(PROFILE_URL, params,
                // success
                function success(data) {
                    
                    $farm_name.text(data[0].farm_name);
                    $coop.text(data[0].coop_name);
                    $member_code.text(data[0].member_code);
                    $("input[name=farm_status][value=" + data[0].status + "]").attr('checked', 'checked');
                    $member_start_date.val(data[0].member_start_date);
                    
                    $fname.val(data[0].farmer_name);
                    $lname.val(data[0].farmer_surname);
                    $sel_title.val(data[0].title_id);
                    $sel_edu.val(data[0].education_level_id);
                    //$("textarea[name=address]").val(data.address);
                    //$("input[name=mobile]").val(data.mobile);
                    $sel_region.val(data[0].region_id);
                    $sel_prov.val(data[0].province_id);
                    $sel_dist.val(data[0].district_id);
                    $sel_sub_dist.val(data[0].sub_district_id);
                    $address.val(data[0].address);
                    $citizen_id.text(data[0].citizen_id);
                    $postcode.val(data[0].post_code);
                    $sel_cow_project.val(data[0].farm_project_id);
                    $location_farmer.val(data[0].latitude+","+data[0].longitude);
                },
                function error(code,desc){

                }
            );
        }
        
        function getFarmerTraining(){
            var params = {access_token: window.localStorage.getItem("access_token"),cmd:"get_all_training"};
            getJsonData(FARMER_TRAINING_URL, params,
                // success
                function success(data) {
                    $.each(data,function(i,e){
                        $sel_farmer_training.append($("<option>").attr('value', e.id).text(e.name));
                    });
                    
                    
                },
                function error(code,desc){

                }
            );
        }
        
        function updateFarmer(){
            var user_id = getUrlParameter("id");
            var title_id = $sel_title.val();
            var fname = $fname.val();
            var lname = $lname.val();
            var edu_id = $sel_edu.val();
            var address = $address.val();
            var region_id = $sel_region.val();
            var prov_id = $sel_prov.val();
            var dist_id = $sel_dist.val();
            var sub_dist_id = $sel_sub_dist.val();
            var postcode = $postcode.val();
            var location_data = $location_farmer.val().split(",");
            var lat = location_data[0];
            var lng = location_data[1];
            var params = {access_token: window.localStorage.getItem("access_token"),cmd:"update_farmer",
                           id:user_id,title_id:title_id,fname:fname,lname:lname,edu_id:edu_id,address:address,
                            region_id:region_id,prov_id:prov_id,dist_id:dist_id,sub_dist_id:sub_dist_id,
                            postcode:postcode,lat:lat,lng:lng};
            getJsonData(PROFILE_URL, params,
                // success
                function success(data) {
                    if(data){
                        alert("แก้ไขข้อมูลเรียบร้อย");
                    }
                    
                    
                },
                function error(code,desc){

                }
            );
        }
        
        function addFarmerTraining(){
            var farmer_id = getUrlParameter("id");
            var farmer_training_id = $sel_farmer_training.val();
            var training_date = $training_date.val();
            var training_place = $training_place.val();
            var training_day = $training_day.val();
            var params = {access_token: window.localStorage.getItem("access_token"),cmd:"add_farmer_training",
                           farmer_id:farmer_id,farmer_training_id:farmer_training_id,training_date:training_date,
                            training_place:training_place,training_day:training_day};
            getJsonData(FARMER_TRAINING_URL, params,
                // success
                function success(data) {
                    console.log(data);
                    if(data){
                        alert("เพิ่มข้อมูลเรียบร้อย");
                        $card_detail.show();
                        $card_training.hide();
                    }
                    
                    
                },
                function error(code,desc){

                }
            );
        }
        
        $btn_save_farm.click(function(){
            updateFarmer();
        });
        
        $btn_save_train.click(function(){
            addFarmerTraining();
        });
        
        $add_traning_btn.click(function(){
            
            $card_detail.hide();
            $card_training.show();
            
        });
        
        $btn_back.click(function(){
            $card_detail.show();
            $card_training.hide();
        });
        
    }
    
    var handleAddPopulationSurvey = function(){
        
        var cmd_action = "add";
        
        getCoop();
        var user_id = getUrlParameter("id");
        
        var $survey_date = $("#survey_date");
        var $farmer_name = $("#farmer_name");
        var $total1 = $("#total1");
        var $total2 = $("#total2");
        var $total3 = $("#total3");
        var $total4 = $("#total4");
        var $total5 = $("#total5");
        var $total6 = $("#total6");
        var $total7 = $("#total7");
        var $total8 = $("#total8");
        var $total9 = $("#total9");
        var $total10 = $("#total10");
        var $total_female = $("#total_female");
        var $total_female_val = $("#total_female_val");
        var $total_male = $("#total_male");
        var $total_cow = $("#total_cow");
        var $total_cow_val = $("#total_cow_val");
        var $remark = $("#remark");
        var $btn_save_statistic = $("#btn_save_statistic");
        
        $("#datetimepicker1").datetimepicker({
            format:"YYYY-MM-DD"
        });
        
        $(".total").keyup(function(){
            
            var sum_cow = parseInt($total1.val())+parseInt($total2.val())+parseInt($total3.val())+parseInt($total4.val())+parseInt($total5.val())
                    +parseInt($total6.val())+parseInt($total7.val())+parseInt($total8.val())+parseInt($total9.val())+parseInt($total10.val());
            if(isNaN(sum_cow)){
                sum_cow = 0;
            }
            $total_female.val(sum_cow);
            $total_female_val.val(sum_cow);
            
            var sum_cow_all = parseInt($total_female_val.val())+parseInt($total_male.val());
            if(isNaN(sum_cow_all)){
                sum_cow_all = 0;
            }
            $total_cow.val(sum_cow_all);
            $total_cow_val.val(sum_cow_all);
            
        });
        
        
        $btn_save_statistic.click(function(){
            if(cmd_action == "add"){
                addPopulationSurvey();
            }else{
                updatePopulationSurvey(); 
            }
            
        });
        
        $('select[name=coop_id]').change(function(){
            var coop_id = $(this).val();
            getFarmer(coop_id,"");
        });
        
        function getCowStatistic(){
            var params = {access_token: window.localStorage.getItem("access_token"),cmd:"get_population_survey_detail",id:user_id};
            getJsonData(POPULATION_SURVEY_URL, params,
                // success
                function success(data) {
                    console.log(data);
                    cmd_action = "edit";
                    $survey_date.val(data.create_date);
                    
                    $total1.val(data.total1);
                    $total2.val(data.total2);
                    $total3.val(data.total3);
                    $total4.val(data.total4);
                    $total5.val(data.total5);
                    $total6.val(data.total6);
                    $total7.val(data.total7);
                    $total8.val(data.total8);
                    $total9.val(data.total9);
                    $total10.val(data.total10);
                    $total_female_val.val(data.total_female);
                    $total_male.val(data.total_male);
                    $total_cow_val.val(data.total_cow);
                    $remark.val(data.remark);
                    
                    getFarmer(data.coop_id,data.farmer_id);
                    $('select[name=coop_id]').val(data.coop_id);
                    
                },
                function error(code,desc){

                }
            );
        }
        
        function getFarmer(coop_id,farmer_id){
            var params = {access_token: window.localStorage.getItem("access_token"),coop_id:coop_id};
            getJsonData(FARMER_URL, params,
                // success
                function success(data) {
                    
                    $('select[name=farmer_name]').html("<option value=''>-- กรุณาเลือก --</option>");
                    $.each(data, function (i, item) {
                        $('select[name=farmer_name]').append($('<option>', {
                            value: item.id,
                            text : item.name+" "+item.surname
                        }));
                    });
                    
                    $farmer_name.val(farmer_id);
                },
                function error(code,desc){

                }
            );
        }
        
        function addPopulationSurvey(){
            
            var survey_date = $survey_date.val();
            var farmer_name = $farmer_name.val();
            var total1 = $total1.val();
            var total2 = $total2.val();
            var total3 = $total3.val();
            var total4 = $total4.val();
            var total5 = $total5.val();
            var total6 = $total6.val();
            var total7 = $total7.val();
            var total8 = $total8.val();
            var total9 = $total9.val();
            var total10 = $total10.val();
            var total_female = $total_female_val.val();
            var total_male = $total_male.val();
            var total_cow = $total_cow_val.val();
            var remark = $remark.val();
            
            var params = {access_token: window.localStorage.getItem("access_token"),cmd:"add_population_survey",
                            survey_date:survey_date,farmer_name:farmer_name,total1:total1,total2:total2,total3:total3,total4:total4,total5:total5
                            ,total6:total6,total7:total7,total8:total8,total9:total9,total10:total10,total_female:total_female,total_male:total_male,
                            total_cow:total_cow,remark:remark};
            getJsonData(POPULATION_SURVEY_URL, params,
                // success
                function success(data) {
                    if(data){
                        alert("เพิ่มข้อมูลเรียบร้อย");
                        location.href = "population_survey.html";
                    }
                    
                },
                function error(code,desc){

                }
            );
            
        }
        
        function updatePopulationSurvey(){
            
            var survey_date = $survey_date.val();
            var farmer_name = $farmer_name.val();
            var total1 = $total1.val();
            var total2 = $total2.val();
            var total3 = $total3.val();
            var total4 = $total4.val();
            var total5 = $total5.val();
            var total6 = $total6.val();
            var total7 = $total7.val();
            var total8 = $total8.val();
            var total9 = $total9.val();
            var total10 = $total10.val();
            var total_female = $total_female_val.val();
            var total_male = $total_male.val();
            var total_cow = $total_cow_val.val();
            var remark = $remark.val();
            
            var params = {access_token: window.localStorage.getItem("access_token"),cmd:"update_population_survey",id:user_id,
                            survey_date:survey_date,farmer_name:farmer_name,total1:total1,total2:total2,total3:total3,total4:total4,total5:total5
                            ,total6:total6,total7:total7,total8:total8,total9:total9,total10:total10,total_female:total_female,total_male:total_male,
                            total_cow:total_cow,remark:remark};
            getJsonData(POPULATION_SURVEY_URL, params,
                // success
                function success(data) {
                    if(data){
                        alert("แก้ไขข้อมูลเรียบร้อย");
                        location.href = "population_survey.html";
                    }
                    
                },
                function error(code,desc){

                }
            );
            
        }
        
        function getCoop(){
            var params = {data: "coop",access_token: window.localStorage.getItem("access_token")};
            getJsonData(MASTER_DATA_URL, params,
                // success
                function success(data) {

                    $.each(data, function (i, item) {
                        $('select[name=coop_id]').append($('<option>', {
                            value: item.id,
                            text : item.name
                        }));
                    });

                    if(user_id != null){ // เป็นการแก้ไข
                        getCowStatistic();
                    }
                },
                function error(code,desc){

                }
            );
        }
        
    }
    
    return {
        init: function(){
            handlePopulationSurvey();
        },
        add: function(){
            handleAddPopulationSurvey();
        },
        detail: function(){
            handlePopulationSurveyDetail();
        }
    }
    
}();
