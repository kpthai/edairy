
/**
 * 
 * 
 * @author Chanwoot  See-ngam <chanwoot2536@gmail.com>
 * @copyright 2017 chanwoot see-ngam
 * @since 1.0
 * 
 */

/**
 * Description of Farmer
 *
 * @author kla
 */
var BreedingService = function(){
    
    var $card_detail = $(".detail");
    var $card_training = $(".training");
    
    var $tb_page = $("#tb_page");
    
    var current_page = 0;
    
    var handleBreedingService = function(){
        $.blockUI({ message: '<h1> กำลังโหลดข้อมูล...</h1>' });
        var $coop_id = $("#coop_id");
        var $farmer_name = $("#farmer_name");
        var $start_date = $("#start_date");
        var $end_date = $("#end_date");
        var $search_btn = $("#search_btn");
    
        getCoop();
        
        //$('#startDate').datetimepicker({format: 'DD/MM/YYYY'});
        //$('#endDate').datetimepicker({format: 'DD/MM/YYYY'});

        $("#add_population").on("click",function(){
                location.href = "add_population_survey.html";
        });
        
        
        function getCoop(){
            var params = {data: "coop",access_token: window.localStorage.getItem("access_token")};
            getJsonData(MASTER_DATA_URL, params,
                // success
                function success(data) {

                    $.each(data, function (i, item) {
                        $('select[name=coop_id]').append($('<option>', {
                            value: item.id,
                            text : item.name
                        }));
                    });

                    $(".sidebar").load("_sidebar.html");

                    $("#cow_list tbody > tr").on("click",function(){
                            window.location = "farm-detail.html";
                    });
                    $.unblockUI();
                    //getDataTable();
                },
                function error(code,desc){

                }
            );
        }

        function getDataTable(){
            var params = {access_token: window.localStorage.getItem("access_token"),farmer_name:$farmer_name.val(),coop_id:$coop_id.val(),
                            start_date:$start_date.val(),end_date:$end_date.val()};
            getJsonData(BREEDING_SERVICE_URL, params,
                // success
                function success(data) {

                    //console.log(data);
                    if(data==null){
                        $("#num_of_result").text("พบ 0 รายการ");
                        $("#cow_list tbody").html("");
                        $.unblockUI();
                        return true;
                    }
                        
                    
                    $("#cow_list tbody").html("");
                    for(var i=0;i< data.length;i++){
                        if(i == (data.length-1)){
                            $("#num_of_result").text("พบ "+data[i].total+" รายการ");
                            var num_page = Math.ceil(data[i].total/20);
                            
                            if(num_page == 1 || num_page == 0){
                                $tb_page.html('<li class="page-item disabled">'+
                                                    '<a class="page-link" href="#" tabindex="-1">Previous</a>'+
                                                  '</li>'+
                                                  '<li class="page-item active"><a class="page-link" href="#">1</a></li>'+
                                                  '<li class="page-item disabled">'+
                                                    '<a class="page-link" href="#">Next</a>'+
                                                  '</li>');
                            }else{
                                $tb_page.html('');
                                if(current_page == 0){
                                    $tb_page.append('<li class="page-item disabled">'+
                                                    '<a class="page-link" href="#" tabindex="-1">First</a>'+
                                                  '</li>'+
                                                    '<li class="page-item disabled">'+
                                                    '<a class="page-link" href="#" tabindex="-1">Previous</a>'+
                                                  '</li>');
                                }else{
                                    $tb_page.append('<li class="page-item">'+
                                                    '<a class="page-link first" href="#" tabindex="-1">First</a>'+
                                                  '</li>'+
                                                  '<li class="page-item">'+
                                                    '<a class="page-link prev" href="#" id="'+current_page+'" tabindex="-1">Previous</a>'+
                                                  '</li>');
                                }
                                var st = 0;
                                if(current_page > 2){
                                    st = (current_page-2);
                                }
                                
                                var last = (5+st);
                                if(last>num_page){
                                    st = num_page-5;
                                    if(st < 0){
                                        st = 0;
                                    }
                                    last = num_page;
                                }
                                
                                for(var j=st;j<last;j++){
                                    if(current_page == j){
                                        $tb_page.append('<li class="page-item active"><a class="page-link" href="#">'+(j+1)+'</a></li>');
                                    }else{
                                        $tb_page.append('<li class="page-item"><a class="page-link page" href="#" id="'+j+'">'+(j+1)+'</a></li>');
                                    }
                                }
                                
                                if(current_page == (num_page-1)){
                                    $tb_page.append('<li class="page-item disabled">'+
                                                        '<a class="page-link" href="#">Next</a>'+
                                                      '</li>'+
                                                      '<li class="page-item disabled">'+
                                                        '<a class="page-link" href="#">Last</a>'+
                                                      '</li>');
                                }else{
                                    $tb_page.append('<li class="page-item">'+
                                                        '<a class="page-link next" href="#" id="'+current_page+'">Next</a>'+
                                                      '</li>'+
                                                      '<li class="page-item">'+
                                                        '<a class="page-link last" href="#" id="'+(num_page-1)+'">Last</a>'+
                                                      '</li>');
                                }
                            }
                            
                        }else{

                            if (data[i].payment_type=='1') {
                                data[i].payment_type = 'เงินสด';
                            }else if (data[i].payment_type=='2') {
                                data[i].payment_type = 'สินเชื่อ';
                            }

                            $("#cow_list tbody").append('\
                                <tr>\
                                    <td>\
                                        <div>'+data[i].run_no+'</div>\
                                        <div id="servicePorvider_Name">'+data[i].name+' '+data[i].surname+'</div>\
                                    </td>\
                                    <td>\
                                        <div>'+data[i].create_date+'</div>\
                                        <div>'+data[i].farm_name+'</div>\
                                    </td>\
                                    <td>\
                                        <div>'+data[i].grand_total+' บาท</div>\
                                        <div>'+data[i].payment_type+'</div>\
                                    </td>\
                                    <td><a href="add_breeding_service.html?id='+data[i].id+'"><i class="fa fa-edit"></i></a></td>\
                                </tr>\
                            ');
                        }
                        
                        $.unblockUI();
                    }
                },
                function error(code,desc){

                }
            );
        }
        
        $("ul").delegate("a.prev","click",function(){
            $.blockUI({ message: '<h1> กำลังโหลดข้อมูล...</h1>' });
            var page = $(this).attr("id");
            if (page >= 1) {
                current_page--;
                getDataTable();
            }
            
        });
        
        $("ul").delegate("a.next","click",function(){
            $.blockUI({ message: '<h1> กำลังโหลดข้อมูล...</h1>' });
                current_page++;
                getDataTable();
        });
        
        $("ul").delegate("a.page","click",function(){
            
            $.blockUI({ message: '<h1> กำลังโหลดข้อมูล...</h1>' });
            var page = $(this).attr("id");
            current_page = page;
            getDataTable();
        });
        
        $("ul").delegate("a.first","click",function(){
            
            $.blockUI({ message: '<h1> กำลังโหลดข้อมูล...</h1>' });
            current_page = 0;
            getDataTable();
        });
        
        $("ul").delegate("a.last","click",function(){
            
            $.blockUI({ message: '<h1> กำลังโหลดข้อมูล...</h1>' });
            var page = $(this).attr("id");
            current_page = page;
            getDataTable();
        });
        
        $search_btn.click(function(){
            getDataTable();
        });
        
    }
    
    var handleBreedingServiceDetail = function(){
        
        //$('#datetimepicker1').datetimepicker({format: 'DD/MM/YYYY'});
        
        getRegion();
        getCowProject();
        getFarmerTraining();
        
        function getRegion(){
            var params = {access_token: window.localStorage.getItem("access_token")};
            getJsonData(REGION_URL, params,
                // success
                function success(data) {
                    $.each(data,function(i,e){
                        $sel_region.append($("<option>").attr('value', e.id).text(e.name));
                    });
                    getProvince();
                },
                function error(code,desc){

                }
            );
        }
        
        function getProvince(){
            var params = {access_token: window.localStorage.getItem("access_token")};
            getJsonData(PROVINCE_URL, params,
                // success
                function success(data) {
                    $.each(data,function(i,e){
                        $sel_prov.append($("<option>").attr('value', e.id).text(e.name));
                    });
                    getDistrict();
                },
                function error(code,desc){

                }
            );
        }
        
        function getDistrict(){
             var params = {access_token: window.localStorage.getItem("access_token")};
            getJsonData(DISTRICT_URL, params,
                // success
                function success(data) {
                    $.each(data,function(i,e){
                        $sel_dist.append($("<option>").attr('value', e.id).text(e.name));
                    });
                    getSubDistrict();
                },
                function error(code,desc){

                }
            );
        }
        
        function getSubDistrict(){
             var params = {access_token: window.localStorage.getItem("access_token")};
            getJsonData(SUB_DISTRICT_URL, params,
                // success
                function success(data) {
                    $.each(data,function(i,e){
                        $sel_sub_dist.append($("<option>").attr('value', e.id).text(e.name));
                    });
                    getFarmerDetail();
                    
                },
                function error(code,desc){

                }
            );
        }
        
        function getCowProject(){
             var params = {access_token: window.localStorage.getItem("access_token")};
            getJsonData(COW_PROJECT_URL, params,
                // success
                function success(data) {
                    $.each(data,function(i,e){
                        $sel_cow_project.append($("<option>").attr('value', e.id).text(e.name));
                    });
                    getFarmerDetail();
                    
                },
                function error(code,desc){

                }
            );
        }
        
        function getFarmerDetail(){
            
            var user_id = getUrlParameter("id");
        
            var params = {access_token: window.localStorage.getItem("access_token"),id: user_id};
            getJsonData(PROFILE_URL, params,
                // success
                function success(data) {
                    
                    $farm_name.text(data[0].farm_name);
                    $coop.text(data[0].coop_name);
                    $member_code.text(data[0].member_code);
                    $("input[name=farm_status][value=" + data[0].status + "]").attr('checked', 'checked');
                    $member_start_date.val(data[0].member_start_date);
                    
                    $fname.val(data[0].farmer_name);
                    $lname.val(data[0].farmer_surname);
                    $sel_title.val(data[0].title_id);
                    $sel_edu.val(data[0].education_level_id);
                    //$("textarea[name=address]").val(data.address);
                    //$("input[name=mobile]").val(data.mobile);
                    $sel_region.val(data[0].region_id);
                    $sel_prov.val(data[0].province_id);
                    $sel_dist.val(data[0].district_id);
                    $sel_sub_dist.val(data[0].sub_district_id);
                    $address.val(data[0].address);
                    $citizen_id.text(data[0].citizen_id);
                    $postcode.val(data[0].post_code);
                    $sel_cow_project.val(data[0].farm_project_id);
                    $location_farmer.val(data[0].latitude+","+data[0].longitude);
                },
                function error(code,desc){

                }
            );
        }
        
        function getFarmerTraining(){
            var params = {access_token: window.localStorage.getItem("access_token"),cmd:"get_all_training"};
            getJsonData(FARMER_TRAINING_URL, params,
                // success
                function success(data) {
                    $.each(data,function(i,e){
                        $sel_farmer_training.append($("<option>").attr('value', e.id).text(e.name));
                    });
                    
                    
                },
                function error(code,desc){

                }
            );
        }
        
        function updateFarmer(){
            var user_id = getUrlParameter("id");
            var title_id = $sel_title.val();
            var fname = $fname.val();
            var lname = $lname.val();
            var edu_id = $sel_edu.val();
            var address = $address.val();
            var region_id = $sel_region.val();
            var prov_id = $sel_prov.val();
            var dist_id = $sel_dist.val();
            var sub_dist_id = $sel_sub_dist.val();
            var postcode = $postcode.val();
            var location_data = $location_farmer.val().split(",");
            var lat = location_data[0];
            var lng = location_data[1];
            var params = {access_token: window.localStorage.getItem("access_token"),cmd:"update_farmer",
                           id:user_id,title_id:title_id,fname:fname,lname:lname,edu_id:edu_id,address:address,
                            region_id:region_id,prov_id:prov_id,dist_id:dist_id,sub_dist_id:sub_dist_id,
                            postcode:postcode,lat:lat,lng:lng};
            getJsonData(PROFILE_URL, params,
                // success
                function success(data) {
                    if(data){
                        alert("แก้ไขข้อมูลเรียบร้อย");
                    }
                    
                    
                },
                function error(code,desc){

                }
            );
        }
        
        function addFarmerTraining(){
            var farmer_id = getUrlParameter("id");
            var farmer_training_id = $sel_farmer_training.val();
            var training_date = $training_date.val();
            var training_place = $training_place.val();
            var training_day = $training_day.val();
            var params = {access_token: window.localStorage.getItem("access_token"),cmd:"add_farmer_training",
                           farmer_id:farmer_id,farmer_training_id:farmer_training_id,training_date:training_date,
                            training_place:training_place,training_day:training_day};
            getJsonData(FARMER_TRAINING_URL, params,
                // success
                function success(data) {
                    console.log(data);
                    if(data){
                        alert("เพิ่มข้อมูลเรียบร้อย");
                        $card_detail.show();
                        $card_training.hide();
                    }
                    
                    
                },
                function error(code,desc){

                }
            );
        }
        
        $btn_save_farm.click(function(){
            updateFarmer();
        });
        
        $btn_save_train.click(function(){
            addFarmerTraining();
        });
        
        $add_traning_btn.click(function(){
            
            $card_detail.hide();
            $card_training.show();
            
        });
        
        $btn_back.click(function(){
            $card_detail.show();
            $card_training.hide();
        });
        
    }
    
    var handleAddBreedingService = function(){
        var ism_id = getUrlParameter("id");
        
        var $pay_no = $("#pay_no");
        var $pay_no_hidden = $("#pay_no_hidden");
        var $pay_date = $("#pay_date");
        var $total_money = $("#total_money");
        var $service_provider = $("#service_provider");
        var $customer = $("#customer");
        
        var breeding_id;
        
        getCoop();
        
        var $farmer_name = $("#farmer_name");
        
        function getCoop(){
            var params = {data: "coop",access_token: window.localStorage.getItem("access_token")};
            getJsonData(MASTER_DATA_URL, params,
                // success
                function success(data) {

                    $.each(data, function (i, item) {
                        $('select[name=coop_id]').append($('<option>', {
                            value: item.id,
                            text : item.name
                        }));
                    });

                    if(ism_id != null){ // เป็นการแก้ไข
                        getBreedData(ism_id);
                    }else{
                        addBreedingService();
                    }
                },
                function error(code,desc){

                }
            );
        }
        
        function getFarmer(coop_id,farmer_id){
            var params = {access_token: window.localStorage.getItem("access_token"),coop_id:coop_id};
            getJsonData(FARMER_URL, params,
                // success
                function success(data) {
                    
                    $('select[name=farmer_name]').html("<option value=''>-- กรุณาเลือก --</option>");
                    $.each(data, function (i, item) {
                        $('select[name=farmer_name]').append($('<option>', {
                            value: item.id,
                            text : item.name+" "+item.surname
                        }));
                    });
                    
                    $farmer_name.val(farmer_id);
                    if(farmer_id!=null && farmer_id!=""){
                        var name = $('select[name=farmer_name] option:selected').text();
                        $customer.text(" "+name);
                    }
                },
                function error(code,desc){

                }
            );
        }
        
        function getBreedData(id){
            var params = {access_token: window.localStorage.getItem("access_token"),cmd:"get_breed_list",id:id};
            getJsonData(BREEDING_DATA_URL, params,
                // success
                function success(data) {
                    console.log(data);
                    if(data==null){
                        $("#num_of_result").text("พบ 0 รายการ");
                        $("#cow_list tbody").html("");
                        return true;
                    }else{
                        $("#num_of_result").text("พบ "+data.length+" รายการ");
                    }
                    $("#cow_list tbody").html("");
                    var tt = 0;
                    for(var i=0;i< data.length;i++){

                        if (data[i].payment_type=='1') {
                            data[i].payment_type = 'เงินสด';
                        }else if (data[i].payment_type=='2') {
                            data[i].payment_type = 'สินเชื่อ';
                        }
                        
                        var img = "1.jpg";
                        if(data[i].image_url != "" && data[i].image_url != null){
                            img = data[i].image_url;
                        }

                        $("#cow_list tbody").append('\
                            <tr>\
                                <td class="text-center">\
                                    <div class="avatar">\
                                            <img src="img/avatars/'+img+'" class="img-avatar" alt="data[i].name">\
                                        <span class="avatar-status"></span>\
                                    </div>\
                                </td>\
                                <td>\
                                    <div>'+data[i].cow_name+'</div>\
                                </td>\
                                <td>\
                                    <div>'+data[i].cow_status+'</div>\
                                </td>\
                                <td>\
                                    <div>'+data[i].total_price+' บาท</div>\
                                </td>\
                                <td><a href="add_breeding_datail.html?id='+id+'&detail_id='+data[i].isml_id+'"><i class="fa fa-edit"></i></a></td>\
                            </tr>\
                        ');
                        
                        tt += parseFloat(data[i].total_price);
                    }
                    
                    $total_money.text(tt.toFixed(2));
                    
                    getFarmerDetail(data[0].created_by);
                    getFarmer(data[0].coop_id,data[0].farmer_id);
                    
                    $pay_no.val(data[0].run_no);
                    $pay_no_hidden.val(data[0].run_no);
                    $pay_date.val(data[0].pay_date);
                    console.log(data[0].ism_payment_type);
                    $("input[name=pay_type][value=" + data[0].ism_payment_type + "]").attr('checked', 'checked');
                    
                    $('select[name=coop_id]').val(data[0].coop_id);
                },
                function error(code,desc){

                }
            );
        }
        
        function addBreedingService(){
            var params = {access_token: window.localStorage.getItem("access_token"),cmd:"add_breeding_service"};
            getJsonData(BREEDING_DATA_URL, params,
                // success
                function success(data) {
                    breeding_id = data;
                    getBreedData(data);
                },
                function error(code,desc){

                }
            );
        }
        
        function getFarmerDetail(farmer_name){
        
            var params = {access_token: window.localStorage.getItem("access_token"),id: farmer_name};
            getJsonData(PROFILE_URL, params,
                // success
                function success(data) {
                    console.log(data);
                    //getCow(data[0].farm_id,"");
                    $service_provider.text(" "+data[0].name+" "+data[0].surname);
                    
                },
                function error(code,desc){

                }
            );
        }
        
        function updateBreedingService(){
            
            var pay_no_hidden = $pay_no_hidden.val();
            var pay_date = $pay_date.val();
            var pay_type = $('input[name=pay_type]').val();
            var total_money = $total_money.text();
            var farmer_name = $('select[name=farmer_name]').val();
            var coop_id = $('select[name=coop_id]').val();
            
            var params = {access_token: window.localStorage.getItem("access_token"),cmd:"edit_breeding_service",
                            id:ism_id,pay_no:pay_no_hidden,pay_date:pay_date,pay_type:pay_type,total_money:total_money,
                            farmer_name:farmer_name,coop_id:coop_id};
            getJsonData(BREEDING_DATA_URL, params,
                // success
                function success(data) {
                    location.href = "breeding_service.html";
                },
                function error(code,desc){

                }
            );
        }
        
        
        $('select[name=coop_id]').change(function(){
            var coop_id = $(this).val();
            getFarmer(coop_id,"");
        });
        
        $('select[name=farmer_name]').change(function(){
            var name = $('select[name=farmer_name] option:selected').text();
            $customer.text(" "+name);
        });
        
        $("#save_btn").on("click",function(){

                //$("#myModal").modal("show");
                updateBreedingService();
        });

        $("#approve").on("click",function(){
                $("#myModal").modal("hide");
                $("#timeUp").modal("show");
        });

        $("#go_survey").on("click",function(){
                location.href = "status_survey.html";
        })

        $("#add_new").on("click",function(){
                if(breeding_id == null){
                    location.href = "add_breeding_datail.html?id="+ism_id;
                }else{
                    location.href = "add_breeding_datail.html?id="+breeding_id;
                }
        });

        $("#reject").on("click",function(){
                location.href = "status_selection.html";
        });

        $("#skip").on("click",function(){
                location.href = "breeding_service.html";
        });

        $("input[type='checkbox']").on("change",function(){
                $("input[type='checkbox']").prop("checked",false);
                $(this).prop("checked",true);
        });

        
        
    }
    
    var handleAddBreedingServiceDetail = function(){
        var cmd_action = "add";
        
        var $farmer_name = $("#farmer_name");
        var $cow_id = $("#cow_id");
        var $ism_time = $("#ism_time");
        var $lot_no = $("#lot_no");
        var $amount = $("#amount");
        var $price = $("#price");
        var $total_price = $("#total_price");
        var $accept = $("#accept");
        var $back = $("#back");
        
        var ism_id = getUrlParameter("id");
        var detail_id = getUrlParameter("detail_id");
        
        getCoop();
         function getCoop(){
            var params = {data: "coop",access_token: window.localStorage.getItem("access_token")};
            getJsonData(MASTER_DATA_URL, params,
                // success
                function success(data) {

                    $.each(data, function (i, item) {
                        $('select[name=coop_id]').append($('<option>', {
                            value: item.id,
                            text : item.name
                        }));
                    });
                    
                    if(detail_id != null && detail_id != ""){
                        getBreedingDetail(detail_id);
                    }
                },
                function error(code,desc){

                }
            );
        }
        
        function getFarmer(coop_id,farmer_id){
            var params = {access_token: window.localStorage.getItem("access_token"),coop_id:coop_id};
            getJsonData(FARMER_URL, params,
                // success
                function success(data) {
                    
                    $('select[name=farmer_name]').html("<option value=''>-- กรุณาเลือก --</option>");
                    $.each(data, function (i, item) {
                        $('select[name=farmer_name]').append($('<option>', {
                            value: item.id,
                            text : item.name+" "+item.surname
                        }));
                    });
                    
                    $farmer_name.val(farmer_id);
                },
                function error(code,desc){

                }
            );
        }
        
        function getCow(farm_id,cow_id){
            var coop_id = $('select[name=coop_id]').val();
            var params = {access_token: window.localStorage.getItem("access_token"),farm_id:farm_id,coop_id:coop_id};
            getJsonData(COW_URL, params,
                // success
                function success(data) {
                    console.log(data);
                    if(data != null){
                        $('select[name=cow_id]').html("<option value=''>-- กรุณาเลือก --</option>");
                        $.each(data, function (i, item) {
                            $('select[name=cow_id]').append($('<option>', {
                                value: item.id,
                                text : item.name
                            }));
                        });
                        console.log(cow_id);
                         $cow_id.val(cow_id);
                    }
                    
                    //$farmer_name.val(farmer_id);
                },
                function error(code,desc){

                }
            );
        }
        
        function getFarmerDetail(farmer_name){
        
            var params = {access_token: window.localStorage.getItem("access_token"),id: farmer_name};
            getJsonData(PROFILE_URL, params,
                // success
                function success(data) {
                    console.log(data[0]);
                    getCow(data[0].farm_id,"");
                },
                function error(code,desc){

                }
            );
        }
        
        function addBreedingDetail(){
            var cow_id = $cow_id.val();
            var men_interval = $('input[name=ism_interval]').val();
            var insemination_time = $ism_time.val();
            var lot_no = $lot_no.val();
            var amount = $amount.val();
            var price = $price.val();
            var total_price = $total_price.text();
            
            var params = {access_token: window.localStorage.getItem("access_token"),cmd:"add_breeding_detail",cow_id: cow_id,men_interval:men_interval,
                        insemination_time:insemination_time,lot_no:lot_no,amount:amount,price:price,total_price:total_price,cow_insemination_id:ism_id};
            getJsonData(BREEDING_DATA_URL, params,
                // success
                function success(data) {
                   
                   location.href = "add_breeding_service.html?id="+ism_id;
                },
                function error(code,desc){

                }
            );
        }
        
        function editBreedingDetail(){
            var cow_id = $cow_id.val();
            var men_interval = $('input[name=ism_interval]').val();
            var insemination_time = $ism_time.val();
            var lot_no = $lot_no.val();
            var amount = $amount.val();
            var price = $price.val();
            var total_price = $total_price.text();
            
            var params = {access_token: window.localStorage.getItem("access_token"),cmd:"edit_breeding_detail",id:detail_id,cow_id: cow_id,men_interval:men_interval,
                        insemination_time:insemination_time,lot_no:lot_no,amount:amount,price:price,total_price:total_price,cow_insemination_id:ism_id};
            getJsonData(BREEDING_DATA_URL, params,
                // success
                function success(data) {
                   
                   location.href = "add_breeding_service.html?id="+ism_id;
                },
                function error(code,desc){

                }
            );
        }
        
        function getBreedingDetail(id){
            var params = {access_token: window.localStorage.getItem("access_token"),cmd:"get_breeding_detail",id:id};
            getJsonData(BREEDING_DATA_URL, params,
                // success
                function success(data) {
                    cmd_action = "edit";
                    $('.srch').hide();
                    $("input[name=ism_interval][value=" + data.men_interval + "]").attr('checked', 'checked');
                    $ism_time.val(data.insemination_time);
                    $lot_no.val(data.lot_no);
                    $amount.val(data.amount);
                    $price.val(data.price);
                    $total_price.text(data.total_price);
                    
                    getCow(data.cfarm_id,data.cow_id);
                   
                },
                function error(code,desc){

                }
            );
        }
        
        $('select[name=coop_id]').change(function(){
            var coop_id = $(this).val();
            getFarmer(coop_id,"");
        });
        
        $('select[name=farmer_name]').change(function(){
            var farmer_name = $(this).val();
            getFarmerDetail(farmer_name);
        });
        
        $amount.keyup(function(){
            var total = parseInt($amount.val())*parseInt($price.val());
            if(!isNaN(total)){
                $total_price.text(total);
            }else{
                $total_price.text(0);
            }
        });
        
        $price.keyup(function(){
            var total = parseInt($amount.val())*parseInt($price.val());
            if(!isNaN(total)){
                $total_price.text(total);
            }else{
                $total_price.text(0);
            }
        });
        
        $accept.click(function(){
            if(cmd_action == "add"){
                addBreedingDetail();
            }else{
                editBreedingDetail();
            }
        });
        
        $back.click(function(){
            
        });
        
        
    }
    
    return {
        init: function(){
            handleBreedingService();
        },
        detail: function(){
            handleBreedingServiceDetail();
        },
        add: function(){
            handleAddBreedingService();
        },
        addDetail: function(){
            handleAddBreedingServiceDetail();
        }
    }
    
}();
