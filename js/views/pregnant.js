
/**
 * 
 * 
 * @author Chanwoot  See-ngam <chanwoot2536@gmail.com>
 * @copyright 2017 chanwoot see-ngam
 * @since 1.0
 * 
 */

/**
 * Description of Farmer
 *
 * @author kla
 */
var Pregnant = function(){
    
    var $card_detail = $(".detail");
    var $card_training = $(".training");
    
   
    var $tb_page = $("#tb_page");
    
    var current_page = 0;
    
    var handlePregnant = function(){
        $.blockUI({ message: '<h1> กำลังโหลดข้อมูล...</h1>' });
        var $coop_id = $("#coop_id");
        var $farmer_name = $("#farmer_name");
        var $start_date = $("#start_date");
        var $end_date = $("#end_date");
        var $search_btn = $("#search_btn");
    
        getCoop();
        
        //$('#startDate').datetimepicker({format: 'DD/MM/YYYY'});
        //$('#endDate').datetimepicker({format: 'DD/MM/YYYY'});

        $("#add_breedind").on("click",function(){
                location.href = "add_breeding_service.html";
        });
        
        $('#startDate').datetimepicker({format: 'DD/MM/YYYY'});
        $('#endDate').datetimepicker({format: 'DD/MM/YYYY'});
        
        function getCoop(){
            var params = {data: "coop",access_token: window.localStorage.getItem("access_token")};
            getJsonData(MASTER_DATA_URL, params,
                // success
                function success(data) {

                    $.each(data, function (i, item) {
                        $('select[name=coop_id]').append($('<option>', {
                            value: item.id,
                            text : item.name
                        }));
                    });

                    $(".sidebar").load("_sidebar.html");

                    $("#cow_list tbody > tr").on("click",function(){
                            window.location = "farm-detail.html";
                    });
                    $.unblockUI();
                    //getDataTable();
                },
                function error(code,desc){

                }
            );
        }

        function getDataTable(){
            var params = {access_token: window.localStorage.getItem("access_token"),farmer_name:$farmer_name.val(),coop_id:$coop_id.val()};
            getJsonData(PREGNANT_URL, params,
                // success
                function success(data) {
                    if(data==null){
                        $("#num_of_result").text("พบ 0 รายการ");
                        $("#cow_list tbody").html("");
                        $.unblockUI();
                        return true;
                    }
                    
                    $("#cow_list tbody").html("");
                    
                    for(var i = 0; i < data.length ; i++){
                        if(i == (data.length-1)){
                            //paging navigator
                            $("#num_of_result").html("ผลการค้นหา "+data[i].total+" รายการ");
                            
                            var num_page = Math.ceil(data[i].total/20);
                            if(num_page == 1 || num_page == 0){
                                $tb_page.html('<li class="page-item disabled">'+
                                                    '<a class="page-link" href="#" tabindex="-1">Previous</a>'+
                                                  '</li>'+
                                                  '<li class="page-item active"><a class="page-link" href="#">1</a></li>'+
                                                  '<li class="page-item disabled">'+
                                                    '<a class="page-link" href="#">Next</a>'+
                                                  '</li>');
                            }else{
                                $tb_page.html('');
                                if(current_page == 0){
                                    $tb_page.append('<li class="page-item disabled">'+
                                                    '<a class="page-link" href="#" tabindex="-1">First</a>'+
                                                  '</li>'+
                                                    '<li class="page-item disabled">'+
                                                    '<a class="page-link" href="#" tabindex="-1">Previous</a>'+
                                                  '</li>');
                                }else{
                                    $tb_page.append('<li class="page-item">'+
                                                    '<a class="page-link first" href="#" tabindex="-1">First</a>'+
                                                  '</li>'+
                                                  '<li class="page-item">'+
                                                    '<a class="page-link prev" href="#" id="'+current_page+'" tabindex="-1">Previous</a>'+
                                                  '</li>');
                                }
                                var st = 0;
                                if(current_page > 2){
                                    st = (current_page-2);
                                }
                                
                                var last = (5+st);
                                if(last>num_page){
                                    st = num_page-5;
                                    if(st < 0){
                                        st = 0;
                                    }
                                    last = num_page;
                                }
                                
                                for(var j=st;j<last;j++){
                                    if(current_page == j){
                                        $tb_page.append('<li class="page-item active"><a class="page-link" href="#">'+(j+1)+'</a></li>');
                                    }else{
                                        $tb_page.append('<li class="page-item"><a class="page-link page" href="#" id="'+j+'">'+(j+1)+'</a></li>');
                                    }
                                }
                                
                                if(current_page == (num_page-1)){
                                    $tb_page.append('<li class="page-item disabled">'+
                                                        '<a class="page-link" href="#">Next</a>'+
                                                      '</li>'+
                                                      '<li class="page-item disabled">'+
                                                        '<a class="page-link" href="#">Last</a>'+
                                                      '</li>');
                                }else{
                                    $tb_page.append('<li class="page-item">'+
                                                        '<a class="page-link next" href="#" id="'+current_page+'">Next</a>'+
                                                      '</li>'+
                                                      '<li class="page-item">'+
                                                        '<a class="page-link last" href="#" id="'+(num_page-1)+'">Last</a>'+
                                                      '</li>');
                                }
                            }
                            
                        }else{
                            
                            $("#cow_list tbody").append('\
                                        <tr>\
                                                <td id="result_detail">\
                                                        <div>'+data[i].cow_id+' '+data[i].cowName+'</div>\
                                                        <div>วันที่ผสม</div>\
                                                </td>\
                                                <td id="result_date">\
                                                        <div class="col-12" style="text-align:center;">วันที่</div>\
                                                        <div >'+data[i].insemination_date+'</div>\
                                                </td>\
                                                <td><a href="pregnant_check.html?id='+data[i].cpiID+'"><i class="fa fa-edit"></i></a></td>\
                                        </tr>\
                            ');

                            var proportion = [{
                                    "startDate" : data[i].date1,
                                    "endDate" : data[i].date1_to
                            },{
                                    "startDate" : data[i].date2,
                                    "endDate" : data[i].date2_to
                            },{
                                    "startDate" : data[i].date3,
                                    "endDate" : data[i].date3_to
                            }];


                            for(var j = 0; j < proportion.length ; j++){
                                    var sDate = proportion[j].startDate;
                                    var eDate = proportion[j].endDate;
                                $("#cow_list tbody tr td#result_detail").last().append('\
                                        <div>กลับสัดครั้งที่'+(j+1)+'</div>\
                                                                                   ');

                                $("#cow_list tbody tr td#result_date").last().append('\
                                        <div>'+sDate+'-'+eDate+'</div>\
                                                                                   ');
                            }
                        }
                    }
                    
                    $.unblockUI();
                },
                function error(code,desc){

                }
            );
        }
        
        $("ul").delegate("a.prev","click",function(){
            $.blockUI({ message: '<h1> กำลังโหลดข้อมูล...</h1>' });
            var page = $(this).attr("id");
            if (page >= 1) {
                current_page--;
                getDataTable();
            }
            
        });
        
        $("ul").delegate("a.next","click",function(){
            $.blockUI({ message: '<h1> กำลังโหลดข้อมูล...</h1>' });
                current_page++;
                getDataTable();
        });
        
        $("ul").delegate("a.page","click",function(){
            
            $.blockUI({ message: '<h1> กำลังโหลดข้อมูล...</h1>' });
            var page = $(this).attr("id");
            current_page = page;
            getDataTable();
        });
        
        $("ul").delegate("a.first","click",function(){
            
            $.blockUI({ message: '<h1> กำลังโหลดข้อมูล...</h1>' });
            current_page = 0;
            getDataTable();
        });
        
        $("ul").delegate("a.last","click",function(){
            
            $.blockUI({ message: '<h1> กำลังโหลดข้อมูล...</h1>' });
            var page = $(this).attr("id");
            current_page = page;
            getDataTable();
        });
        
        $search_btn.click(function(){
            $.blockUI({ message: '<h1> กำลังโหลดข้อมูล...</h1>' });
            current_page = 0;
            getDataTable();
        });
        
    }
    
    
    var handleAddPregnant = function(){
        var id = getUrlParameter("id");
        
        var $cow_data = $("#cow_data");
        var $check_date = $("#check_date");
        
        $('#datetimepicker1').datetimepicker({format: 'YYYY-MM-DD'});
        
        getPregnantDetail();
        
        function getPregnantDetail(){
        
            var params = {access_token: window.localStorage.getItem("access_token"),cmd:"get_pregnant_detail",id: id};
            getJsonData(PREGNANT_URL, params,
                // success
                function success(data) {
                    console.log(data);
                    //getCow(data[0].farm_id,"");
                    $cow_data.text(data.cow_id+" "+data.cowName);
                    if(data.create_date == null){
                        $check_date.val("");
                    }else{
                        $check_date.val(data.create_date);
                    }
                    
                    $("input[name=result][value=" + data.inspection_result + "]").attr('checked', 'checked');
                    
                    getBreedingDetail(data.cow_insemination_line_id);
                    
                },
                function error(code,desc){

                }
            );
        }
        
        function getBreedingDetail(id){
            var params = {access_token: window.localStorage.getItem("access_token"),cmd:"get_breeding_detail",id:id};
            getJsonData(BREEDING_DATA_URL, params,
                // success
                function success(data) {
                    
                    var date = new Date(data.create_date);
                    
                    var stdate = new Date(date.setDate(date.getDate() + 210));
                    
                    var bdate = new Date(date.setDate(date.getDate() + 270));
                    
                    var pad = "00";
                    
                    var stdmonth = "" + (stdate.getMonth()+1);
                    var ansstdmonth = pad.substring(0, pad.length - stdmonth.length) + stdmonth;
                    var bmonth = "" + (bdate.getMonth()+1);
                    var ansbmonth = pad.substring(0, pad.length - bmonth.length) + bmonth;
                    
                    var stdday = "" + (stdate.getDate()+1);
                    var ansstdday = pad.substring(0, pad.length - stdday.length) + stdday;
                    var bday = "" + (bdate.getDate()+1);
                    var ansbday = pad.substring(0, pad.length - bday.length) + bday;
                    
                    $("#cow_data_status").text(data.cow_id+" "+data.cow_name);
                    $("#ism_date_status").text(data.create_date);
                    $("#stop_date_status").text(stdate.getFullYear()+"-"+ansstdmonth+"-"+ansstdday);
                    $("#birth_date_status").text(bdate.getFullYear()+"-"+ansbmonth+"-"+ansbday);
                   
                },
                function error(code,desc){

                }
            );
        }
        
        function updatePregnant(){
            
            var check_date = $check_date.val();
            var result = $('input[name=result]:checked').val();
            
            var params = {access_token: window.localStorage.getItem("access_token"),cmd:"edit_pregnant",
                            id:id,check_date:check_date,result:result};
            getJsonData(PREGNANT_URL, params,
                // success
                function success(data) {
                   location.href = "pregnant.html";
                },
                function error(code,desc){

                }
            );
    
        }
        
        $("input[type='checkbox']").on("change",function(){

            var value = $(this).attr("value");
            if(value == "1"){
                    $("#result1").attr("checked", true);
                    $("#result2").attr("checked", false);
            }else{
                    $("#result1").attr("checked", false);
                    $("#result2").attr("checked", true);
            }
	});
        
        $("#submit_result_btn").on("click",function(){
            if($('input[name=result]').val() === "1"){
                $("#myModal").modal("show");
            }else{
                updatePregnant();
            }
        });

        $("#cloas_btn").on("click",function(){
            $("#myModal").modal("hide");
            updatePregnant();
        });
        
    }
    
    
    
    return {
        init: function(){
            handlePregnant();
        },
        add: function(){
            handleAddPregnant();
        }
    }
    
}();
