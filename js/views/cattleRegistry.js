/**
 * 
 * 
 * @author Chanwoot  See-ngam <chanwoot2536@gmail.com>
 * @copyright 2017 chanwoot see-ngam
 * @since 1.0
 * 
 */

/**
 * Description of Farmer
 *
 * @author kla
 */
var ImageFile = [];

var CattleRegistry = function(){
    
    var $card_detail = $(".detail");
    var $card_training = $(".training");
    
    var $tb_page = $("#tb_page");
    
    var current_page = 0;
    var handleCattle = function(){
        $.blockUI({ message: '<h1> กำลังโหลดข้อมูล...</h1>' });
        var $coop_id = $("#coop_id");
        var $farmer_name = $("#farmer_name");
        var $cow_name = $("#cow_name");
        var $search_btn = $("#search_btn");
        
        getCoop();
        
        function getCoop(){
            var params = {data: "coop",access_token: window.localStorage.getItem("access_token")};
            getJsonData(MASTER_DATA_URL, params,
                // success
                function success(data) {

                    $.each(data, function (i, item) {
                        $('select[name=coop_id]').append($('<option>', {
                            value: item.id,
                            text : item.name
                        }));
                    });

                    $(".sidebar").load("_sidebar.html");

                    $("#cow_list tbody > tr").on("click",function(){
                            window.location = "farm-detail.html";
                    });
                    $.unblockUI();
                    //getDataTable();
                },
                function error(code,desc){

                }
            );
        }

        function getDataTable(){
            var params = {access_token: window.localStorage.getItem("access_token"),farmer_name:$farmer_name.val(),coop_id:$coop_id.val(),cow_name:$cow_name.val(),current_page:current_page};
            getJsonData(COW_URL, params,
                // success
                function success(data) {
                   
                    if(data==null){
                        $("#num_of_result").html("ผลการค้นหา 0 รายการ");
                         $("#cow_list tbody").html("");
                         $.unblockUI();
                        return true;
                    }
                    $("#cow_list tbody").html("");
                    
                    for(var i = 0; i < data.length ; i++){
                        if(i == (data.length-1)){
                            //paging navigator
                            $("#num_of_result").html("ผลการค้นหา "+data[i].total+" รายการ");
                            
                            var num_page = Math.ceil(data[i].total/20);
                            if(num_page == 1 || num_page == 0){
                                $tb_page.html('<li class="page-item disabled">'+
                                                    '<a class="page-link" href="#" tabindex="-1">Previous</a>'+
                                                  '</li>'+
                                                  '<li class="page-item active"><a class="page-link" href="#">1</a></li>'+
                                                  '<li class="page-item disabled">'+
                                                    '<a class="page-link" href="#">Next</a>'+
                                                  '</li>');
                            }else{
                                $tb_page.html('');
                                if(current_page == 0){
                                    $tb_page.append('<li class="page-item disabled">'+
                                                    '<a class="page-link" href="#" tabindex="-1">First</a>'+
                                                  '</li>'+
                                                    '<li class="page-item disabled">'+
                                                    '<a class="page-link" href="#" tabindex="-1">Previous</a>'+
                                                  '</li>');
                                }else{
                                    $tb_page.append('<li class="page-item">'+
                                                    '<a class="page-link first" href="#" tabindex="-1">First</a>'+
                                                  '</li>'+
                                                  '<li class="page-item">'+
                                                    '<a class="page-link prev" href="#" id="'+current_page+'" tabindex="-1">Previous</a>'+
                                                  '</li>');
                                }
                                var st = 0;
                                if(current_page > 2){
                                    st = (current_page-2);
                                }
                                
                                var last = (5+st);
                                if(last>num_page){
                                    st = num_page-5;
                                    if(st < 0){
                                        st = 0;
                                    }
                                    last = num_page;
                                }
                                
                                for(var j=st;j<last;j++){
                                    if(current_page == j){
                                        $tb_page.append('<li class="page-item active"><a class="page-link" href="#">'+(j+1)+'</a></li>');
                                    }else{
                                        $tb_page.append('<li class="page-item"><a class="page-link page" href="#" id="'+j+'">'+(j+1)+'</a></li>');
                                    }
                                }
                                
                                if(current_page == (num_page-1)){
                                    $tb_page.append('<li class="page-item disabled">'+
                                                        '<a class="page-link" href="#">Next</a>'+
                                                      '</li>'+
                                                      '<li class="page-item disabled">'+
                                                        '<a class="page-link" href="#">Last</a>'+
                                                      '</li>');
                                }else{
                                    $tb_page.append('<li class="page-item">'+
                                                        '<a class="page-link next" href="#" id="'+current_page+'">Next</a>'+
                                                      '</li>'+
                                                      '<li class="page-item">'+
                                                        '<a class="page-link last" href="#" id="'+(num_page-1)+'">Last</a>'+
                                                      '</li>');
                                }
                            }
                            
                        }else{
                            var img = "1.jpg";
                            $("#cow_list tbody").append('\
                                <tr>\
                                    <td class="text-center">\
                                        <div class="avatar">\
                                                <img src="img/avatars/'+img+'" class="img-avatar" alt="">\
                                            <span class="avatar-status"></span>\
                                        </div>\
                                    </td>\
                                    <td>\
                                        <div>'+data[i].name+'<i class="fa fa-venus"></i></div>\
                                        <div class="small text-muted">'+data[i].cow_id+'</div>\
                                    </td>\
                                    <td>'+data[i].cow_status+'</td>\
                                    <td class="text-center"><a href="cattle_detail.html?id='+data[i].id+'"><i class="fa fa-edit"></i></a>\
                                    </td>\
                                </tr>');
                        }
                    }
                    
                    $.unblockUI();
                },
                function error(code,desc){

                }
            );
        }
        
        $("ul").delegate("a.prev","click",function(){
            $.blockUI({ message: '<h1> กำลังโหลดข้อมูล...</h1>' });
            var page = $(this).attr("id");
            if (page >= 1) {
                current_page--;
                getDataTable();
            }
            
        });
        
        $("ul").delegate("a.next","click",function(){
            $.blockUI({ message: '<h1> กำลังโหลดข้อมูล...</h1>' });
                current_page++;
                getDataTable();
        });
        
        $("ul").delegate("a.page","click",function(){
            
            $.blockUI({ message: '<h1> กำลังโหลดข้อมูล...</h1>' });
            var page = $(this).attr("id");
            current_page = page;
            getDataTable();
        });
        
        $("ul").delegate("a.first","click",function(){
            
            $.blockUI({ message: '<h1> กำลังโหลดข้อมูล...</h1>' });
            current_page = 0;
            getDataTable();
        });
        
        $("ul").delegate("a.last","click",function(){
            
            $.blockUI({ message: '<h1> กำลังโหลดข้อมูล...</h1>' });
            var page = $(this).attr("id");
            current_page = page;
            getDataTable();
        });
        
        $search_btn.click(function(){
            $.blockUI({ message: '<h1> กำลังโหลดข้อมูล...</h1>' });
            current_page = 0;
            getDataTable();
        });
        
    }
    
    var handleCattleDetail = function(){
        $.blockUI({ message: '<h1> กำลังโหลดข้อมูล...</h1>' });
        var $image = $("#img_cow");
        
        var $name = $("#name");
        var $cow_id = $("#cow_id");
        var $ear_id = $("#ear_id");
        var $dld_id = $("#dld_id");
        var $breed = $("#breed");
        var $birthdate = $("#birthdate");
        var $cow_age = $("#cow_age");
        var $move_farm_date = $("#move_farm_date");
        var $cow_status = $("#cow_status");
        var $breeders = $("#breeders");
        var $cows = $("#cows");
        var $btn_save_cattle = $("#btn_save_cattle");
        
        
        getBreeder(); //get พ่อพันธ์
        getCows(); //get แม่พันธ์
        getCowsStatus();
        getCattleDetail();
        
        //issue title
        var access_token =  window.localStorage.getItem("access_token");
        var $farm_id = $("#farm_id");
        var ajax_url_farm_id = FARM_URL + "?cmd=qry_farm_id&access_token="+access_token;
        var placeholder_farm_id = $farm_id.attr('placeholder'); 
        initSelect2Ajax($farm_id, placeholder_farm_id, ajax_url_farm_id, false);
        
        var $from_farm_id = $("#from_farm_id");
        var ajax_url_from_farm_id = FARM_URL + "?cmd=qry_farm_id&access_token="+access_token;
        var placeholder_from_farm_id = $from_farm_id.attr('placeholder'); 
        initSelect2Ajax($from_farm_id, placeholder_from_farm_id, ajax_url_from_farm_id, false);
        
        $("#checkout").on("click",function(){
            var id = getUrlParameter('id');
            location.href = "cattle_checkout.html?id="+id;
	});
	
	$("input[type='checkbox']").on("change",function(){
            $("input[type='checkbox']").prop("checked",false);
            $(this).prop("checked",true);

            var id = $(this).attr("id");
            if(id == "buy_cow"){
                    $from_farm_id.prop("disabled", false);
            }else{
                     $from_farm_id.prop("disabled", true);
            }
	});
        
        $btn_save_cattle.click(function(){
            updateCattle();
        });
        
        function getBreeder(){
            var params = {access_token: window.localStorage.getItem("access_token")};
            getJsonData(BREEDER_URL, params,
                // success
                function success(data) {
                    $.each(data, function (i, item) {
                        $breeders.append($('<option>', {
                                value: item.id,
                                text : item.name
                            }));
                    });
                },
                function error(code,desc){

                }
            );
        }
        
        function getCows(){
            var params = {access_token: window.localStorage.getItem("access_token")};
            getJsonData(COW_URL, params,
                // success
                function success(data) {
                    $.each(data, function (i, item) {
                        $cows.append($('<option>', {
                                value: item.id,
                                text : item.name
                            }));
                    });
                },
                function error(code,desc){

                }
            );
        }
        
        function getCowsStatus(){
            var params = {access_token: window.localStorage.getItem("access_token"),cmd:"get_all_cow_status"};
            getJsonData(COW_STATUS_URL, params,
                // success
                function success(data) {
                    $.each(data, function (i, item) {
                        $cow_status.append($('<option>', {
                                value: item.id,
                                text : item.name
                            }));
                    });
                },
                function error(code,desc){

                }
            );
        }
        
        function updateCattle(){
            
            var id = getUrlParameter('id');
            var name = $name.val();
            var cow_id = $cow_id.val();
            var ear_id = $ear_id.val();
            var dld_id = $dld_id.val();
            var breed = $breed.val();
            var birthdate = $birthdate.val();
            var move_farm_date = $move_farm_date.val();
            var move_type = $("input[type='checkbox']:checked").val();
            var from_farm_id = $from_farm_id.val();
            var cow_status = $cow_status.val();
            var params = {access_token: window.localStorage.getItem("access_token"),cmd:"update_cow_detail",id: id,
                            name:name,cow_id:cow_id,ear_id:ear_id,dld_id:dld_id,breed:breed,
                            birthdate:birthdate,move_farm_date:move_farm_date,move_type:move_type,cow_status:cow_status,from_farm_id:from_farm_id};
            getJsonData(COW_URL, params,
                // success
                function success(data) {
                    window.location.href = "./cattle-registry-search.html";
                },
                function error(code,desc){

                }
            );
        }
        
        function getCattleDetail(){
            var id = getUrlParameter('id');
            
            var params = {access_token: window.localStorage.getItem("access_token"),cmd:"get_cow_detail",id: id};
            getJsonData(COW_URL, params,
                // success
                function success(data) {
                    var  image_url = data.image_url;
                    var cow_status_id = data.cow_status_id;
                    if(image_url != ""){
                        $image.attr("src",HOST+"/upload/"+image_url);
                    }
                    $name.val(data.name);
                    $cow_id.val(data.cow_id);
                    $ear_id.val(data.ear_id);
                    $dld_id.val(data.dld_id);
                    $breed.val(data.breed);
                    $birthdate.val(data.birthdate);
                    $move_farm_date.val(data.move_farm_date);
                    if(cow_status_id != ""){
                        $("input[name=move_type][value=" + cow_status_id + "]").attr('checked', 'checked');
                    
                        $cow_status.val(data.cow_status_id);
                    }
                    calculateCowAge(data.birthdate);
                    $breeders.val(data.father_cow_id);
                    $cows.val(data.mother_cow_id);
                    $breed.val(data.breed);
                    //loadCowStatus(data.cow_status_id);
                    
                    $.unblockUI();
                },
                function error(code,desc){

                }
            );
        }
        
        function getAge(dateString) {
            var today = new Date();
            var birthDate = new Date(dateString);
            var age = today.getFullYear() - birthDate.getFullYear();
            var m = today.getMonth() - birthDate.getMonth();
            if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
                age--;
            }
            return age;
        }

        function calculateCowAge(unformattedDateString){
            var dateStringPart = unformattedDateString.split("/");
            $cow_age.val(getAge(dateStringPart[2]-543+"-"+dateStringPart[1]+"-"+dateStringPart[0]));

        }
        
        
        
    }
    
    var handleAddCattle = function(){
        
        //issue title
        var access_token =  window.localStorage.getItem("access_token");
        var $farm_id = $("#farm_id");
        var ajax_url_farm_id = FARM_URL + "?cmd=qry_farm_id&access_token="+access_token;
        var placeholder_farm_id = $farm_id.attr('placeholder'); 
        initSelect2Ajax($farm_id, placeholder_farm_id, ajax_url_farm_id, false);
        
        var $from_farm_id = $("#from_farm_id");
        var ajax_url_from_farm_id = FARM_URL + "?cmd=qry_farm_id&access_token="+access_token;
        var placeholder_from_farm_id = $from_farm_id.attr('placeholder'); 
        initSelect2Ajax($from_farm_id, placeholder_from_farm_id, ajax_url_from_farm_id, false);
        
        getCowsStatus();
        
        var $chk_buy = $("#chk_buy");
        var $chk_birth = $("#chk_birth");
        var $move_type = 2;
        
        $chk_birth.click(function(){
            if($(this).is(":checked")){
                $move_type = 1;
                $chk_buy.removeAttr("checked");
                $(this).attr("checked","checked");
                enableObject($from_farm_id,false);
            }else{
                $(this).attr("checked","checked");
                enableObject($from_farm_id,true);
            }
        });
        
        $chk_buy.click(function(){
            if($(this).is(":checked")){
                $move_type = 2;
                $chk_birth.removeAttr("checked");
                $(this).attr("checked","checked");
                enableObject($from_farm_id,true);
            }else{
                $(this).attr("checked","checked");
                enableObject($from_farm_id,false);
            }
        });
        
        //เพิ่ม
        var $name = $("#name");
        var $cow_id = $("#cow_id");
        var $ear_id = $("#ear_id");
        var $dld_id = $("#dld_id");
        var $birthdate = $("#birthdate");
        var $cow_age = $("#cow_age");
        var $cow_status = $("#cow_status");
        var $breed = $("#breed");
        var $move_farm_date = $("#move_farm_date");
        var $btn_save_cattle = $("#btn_save_cattle");
        
        $("input[type='checkbox']").on("change",function(){
                $("input[type='checkbox']").prop("checked",false);
                $(this).prop("checked",true);

        });
        
        $birthdate.on("change",function(){
            calculateCowAge($(this).val());
        });
        
        $btn_save_cattle.click(function(){
            savecattle();
            
        });
        
        function savecattle() {

            var name = $name.val();
            var cow_id = $cow_id.val();
            var ear_id = $ear_id.val();
            var dld_id = $dld_id.val();
            var birthdate = $birthdate.val();
            var move_farm_date = $move_farm_date.val();
            var move_type = $move_type;
            var cow_status = $cow_status.val();
            var farm_id = $farm_id.val();
            var from_farm_id = $from_farm_id.val();
            var breed = $breed.val();
            var params = {access_token: window.localStorage.getItem("access_token"),
                           name:name,cow_id:cow_id,ear_id:ear_id,dld_id:dld_id,
                            birthdate:birthdate,move_farm_date:move_farm_date,move_type:move_type,
                            cow_status:cow_status,farm_id:farm_id,from_farm_id:from_farm_id,breed:breed};
            getJsonData(ADD_CATTLE_URL, params,
                // success
                function success(data) {
                    if(data){
                        //uploadImg();
                        var $scope = angular.element('body[ng-controller="EdairyPicCtrl"]').scope();
                        $scope.FileUpload();
                    }
                    
                    
                },
                function error(code,desc){

                }
            );
        }
        
        
        function uploadImg(){
            if(ImageFile.length == 0){
                alert("เพิ่มข้อมูลเรียบร้อย");
            }else{
                var formData = new FormData();
                // Attach file
                formData.append('cmd', 'upload_img');
                formData.append('filename', ImageFile); 
                $.ajax({
                    url: ADD_CATTLE_URL,
                    data: formData,
                    type: 'POST',
                    contentType: false, // NEEDED, DON'T OMIT THIS (requires jQuery 1.6+)
                    processData: false,
                    success: function(res){
                            alert("เพิ่มข้อมูลเรียบร้อย");
                    } // NEEDED, DON'T OMIT THIS
                    // ... Other options like success and etc
                });
            }
        }
        
        function getAge(dateString) {
            var today = new Date();
            var birthDate = new Date(dateString);
            var age = today.getFullYear() - birthDate.getFullYear();
            var m = today.getMonth() - birthDate.getMonth();
            if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
                age--;
            }
            return age;
        }

        function calculateCowAge(unformattedDateString){
            var dateStringPart = unformattedDateString.split("/");
            $cow_age.val(getAge(dateStringPart[2]-543+"-"+dateStringPart[1]+"-"+dateStringPart[0]));

        }
        
        function getCowsStatus(){
            var params = {access_token: window.localStorage.getItem("access_token"),cmd:"get_all_cow_status"};
            getJsonData(COW_STATUS_URL, params,
                // success
                function success(data) {
                    $.each(data, function (i, item) {
                        $cow_status.append($('<option>', {
                                value: item.id,
                                text : item.name
                            }));
                    });
                },
                function error(code,desc){

                }
            );
        }
    }
    
    var handleMoveOut = function(){
        var access_token =  window.localStorage.getItem("access_token");
        var $farm_id = $("#farm_id");
        var ajax_url_farm_id = FARM_URL + "?cmd=qry_farm_id&access_token="+access_token;
        var placeholder_farm_id = $farm_id.attr('placeholder'); 
        initSelect2Ajax($farm_id, placeholder_farm_id, ajax_url_farm_id, false);
        
        var $move_out_date = $("#move_out_date");
        var $discard_select1 = $("#discard_select1");
        var $discard_select2 = $("#discard_select2");
        
        function updateMoveOut(){
            
            var id = getUrlParameter('id');
            var move_out_date = $move_out_date.val();
            var move_type = $("input[type='checkbox']:checked").val();
            var farm_id = $farm_id.val();
            var discard_select1 = $discard_select1.val();
            var $discard_select2 = $discard_select2.val();
            var params = {access_token: window.localStorage.getItem("access_token"),cmd:"update_move_out",id: id,
                            move_out_date:move_out_date,move_type:move_type,farm_id:farm_id,discard_select1:discard_select1,$discard_select2:$discard_select2};
            getJsonData(COW_URL, params,
                // success
                function success(data) {
                    window.location.href = "./cattle-registry-search.html";
                },
                function error(code,desc){

                }
            );
        }
        
        $("#submit_btn").on("click",function(){
            updateMoveOut();
		//location.href = "cattle-registry-search.html";
	});
	
	$("input[type='checkbox']").on("change",function(){
		$("input[type='checkbox']").prop("checked",false);
		$(this).prop("checked",true);
		var id = $(this).attr("id");
		if( id == "1"){
			$("#farm_id").prop("disabled",true);
			$("#discard_select1").prop("disabled",true);
			$("#discard_select2").prop("disabled",true);
			$("#discard_des").prop("disabled",true);
		}else if(id == "2"){
			$("#farm_id").prop("disabled",false);
			$("#discard_select1").prop("disabled",true);
			$("#discard_select2").prop("disabled",true);
			$("#discard_des").prop("disabled",true);
		}else{
			$("#farm_id").prop("disabled",true);
			$("#discard_select1").prop("disabled",false);
			$("#discard_select2").prop("disabled",false);
			$("#discard_des").prop("disabled",false);
		}
	});

        var user_id = window.localStorage.getItem("user_id");
            $.post( PROFILE_URL, { id: user_id }, function(data){
                                console.log(data);
                                $("input[name=name]").val(data.name);
                                $("input[name=surname]").val(data.surname);
                                $("textarea[name=address]").val(data.address);
                                $("input[name=mobile]").val(data.mobile);
            });
    }
    
    
    return {
        init: function(){
            handleCattle();
        },
        add: function(){
            handleAddCattle();
        },
        detail: function(){
            handleCattleDetail();
        },
        moveout: function(){
            handleMoveOut();
        }
    }
    
}();
