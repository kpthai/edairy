/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


var Profile = function(){
    
    var handleProfile = function(){
        
        var $btn_save = $("#btn_save");
        var $newpassword = $("#newpassword");
        var $password = $("#password");
        
        var user_id = window.localStorage.getItem("user_id");
        $.post( PROFILE_URL, { id: user_id }, function(data){
                            $("input[name=username]").val(data.data.username);
        });
        
        function updatePassword(){
            var newpassword = $newpassword.val();
            var password = $password.val();
            var params = {access_token: window.localStorage.getItem("access_token"),cmd:"update_password",id: user_id,password:password,newpassword:newpassword};
            getJsonData(PROFILE_URL, params,
                // success
                function success(data) {
                    if(data == true){
                        alert("update password complete");
                    }
                },
                function error(code,desc){

                }
            );
        }
        
        $btn_save.click(function(){
            updatePassword();
        });
    }
    
    return {
        init: function(){
            handleProfile();
        }
    }
    
    
}();