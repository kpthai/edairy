document.addEventListener("deviceready", onDeviceReadyGPS, false);


var sentGPS = false;
var watchID = null;
function onDeviceReadyGPS() {
    //navigator.geolocation.getCurrentPosition(onSuccess, onError);

    var options = { enableHighAccuracy: true };
    watchID = navigator.geolocation.watchPosition(onSuccess, onError, options);
}
// onSuccess Geolocation
    //
    function onSuccess(position) {

           var token = 'Bearer '+ window.localStorage.getItem("token");
 
           var date = new Date();
var day = date.getDate();        // yields day
var month = date.getMonth()+1;    // yields month
var year = date.getFullYear();  // yields year
var hour = date.getHours();     // yields hours 
var minute = date.getMinutes(); // yields minutes
var second = date.getSeconds(); // yields seconds

if(day<10){
    day="0"+day;
}
if(month<10){
    month="0"+month;
}
if(hour<10){
    hour="0"+hour;
}
if(minute<10){
    minute="0"+minute;
}
if(second<10){
    second="0"+second;
}
// After this construct a string with the above results as below
var time = day + "/" + month + "/" + year + " " + hour + ':' + minute + ':' + second; 
            if(position.coords.accuracy>20){
                $.ajax({
                         url: GPS_TRACKING_URL,
                         type: "POST",
                         data: {CREATE_DATETIME: time ,USERNAME: window.localStorage.getItem("username"), WK_CTR: window.localStorage.getItem("WK_CTR"), LAT: position.coords.latitude, LONG: position.coords.longitude, ACCURACY:  position.coords.accuracy, HEADING: position.coords.heading, SPEED: position.coords.speed },
                         beforeSend: function(request) {
                            request.setRequestHeader("Authorzation", token);
                          },
                         success: function(data) { 
                                console.log(data);
                             }
                      });
                sentGPS = true;
                clearWatch();
            }
    }

    // onError Callback receives a PositionError object
    //
    function onError(error) {
       // alert('code: '    + error.code    + '\n' +
            //  'message: ' + error.message + '\n');
    }

    // clear the watch that was started earlier
    //
    function clearWatch() {
        if (watchID != null) {
            navigator.geolocation.clearWatch(watchID);
            watchID = null;
        }
    }
