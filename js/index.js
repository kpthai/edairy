/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
var app = {
    // Application Constructor
    initialize: function() {
        this.bindEvents();
    },
    // Bind Event Listeners
    //
    // Bind any events that are required on startup. Common events are:
    // 'load', 'deviceready', 'offline', and 'online'.
    bindEvents: function() {
        document.addEventListener('deviceready', this.onDeviceReady, false);
    },
    // deviceready Event Handler
    //
    // The scope of 'this' is the event. In order to call the 'receivedEvent'
    // function, we must explicitly call 'app.receivedEvent(...);'
    onDeviceReady: function() {
/*
             
        FCMPlugin.onNotification(
          function(data){
            var result = data;
            if(data.wasTapped){
              //Notification was received on device tray and tapped by the user.
              
              if(window.localStorage.getItem("logged_in")==1){
                window.location="wodetail.html?id="+result.ORDERID;
              }else{
                alert("Receive Work Order No."+result.ORDERID+" , Please Login First");
                window.localStorage.setItem("redirect_after_logged_in", 1);
                window.localStorage.setItem("redirect_url", "wodetail.html?id="+result.ORDERID);
              }
              //alert( JSON.stringify(data) );
            }else{
              //Notification was received in foreground. Maybe the user needs to be notified.
              //alert( JSON.stringify(data) );
              if(window.localStorage.getItem("logged_in")==1){
                  if(confirm("Receive Work Order No."+result.ORDERID+" , Open Now?")){
                    window.location="wodetail.html?id="+result.ORDERID;
                  }
              }else{
                alert("Receive Work Order No."+result.ORDERID+" , Please Login First");
                window.localStorage.setItem("redirect_after_logged_in", 1);
                window.localStorage.setItem("redirect_url", "wodetail.html?id="+result.ORDERID);
              }
            }
          },
          function(msg){
            console.log('onNotification callback successfully registered: ' + msg);
          },
          function(err){
            console.log('Error registering onNotification callback: ' + err);
          }
        );*/

        app.receivedEvent('deviceready');

    },
    // Update DOM on a Received Event
    receivedEvent: function(id) {

        console.log('Received Event: ' + id);
    }
};
