var apps = angular.module("Edairy", ['ngCordova']);
apps.controller("EdairyPicCtrl", function ($scope, $http, $cordovaLaunchNavigator, $location, $cordovaDevice,$compile,$timeout) {
    
    $("#btn_take_pic").click(function(){
        takeAPicture(1);
    });
    
    $("#btn_sel_pic").click(function(){
        takeAPicture(2);
    });
    
    function takeAPicture(srcType){
        navigator.camera.getPicture(onSuccess, onFail, { quality: 50, 
            destinationType: 1,
            sourceType: srcType
        }); 

        function onSuccess(imageURI) {
            ImageFile[0] = imageURI;
            var image = $("#img_cow");
            image.attr("src",imageURI);
            //image.src = imageURI;
            //alert('ok');
        }

        function onFail(message) {
            alert('Failed because: ' + message);
        }
    }
    
    
    $scope.FileUpload = function(){
        var post_url = ADD_CATTLE_URL + "?cmd=upload_img";
            
            var fileURLDATA = ImageFile[0];

            var success = function (r) {
                    alert("เพิ่มข้อมูลเรียบร้อย"); 
                    // displayFileData(fileEntry.fullPath + " (content uploaded to server)");
            }

            var fail = function (error) {
                    //alert("An error has occurred: Code = " + error.code);
            }

            var options = new FileUploadOptions();
            options.fileKey = "file";
            options.fileName = fileURLDATA.substr(fileURLDATA.lastIndexOf('/') + 1);
            options.mimeType = "image/jpeg";
            options.headers = {
               Connection: "close"
            };
            options.chunkedMode = false;

            // setup parameters
            var params = {};
            params.fullpath =fileURLDATA;
            params.name = options.fileName;
            options.params = params;
            var ft = new FileTransfer();
            // SERVER must be a URL that can handle the request, like
            // http://some.server.com/upload.php

            ft.upload(fileURLDATA, encodeURI(post_url), success, fail, options);
            
        
    }
    
});